<?php 
require_once"conexion.php";

class MdlEstudiantes{

    static public  function RegistrarEstudiantes($primer_nombre , $segundo_nombre , $primer_apellido , $segundo_apellido , $cedula , $celular , $correo, $contrasena , $fecha_registro){
        $stmt = Conection::conectar()->prepare("INSERT INTO 
        usuarios (usuario , correo, celular , contrasena, rol, cedula, primer_nombre , segundo_nombre , primer_apellido , segundo_apellido , fecha_registro, estado) 
        VALUES (:usuario , :correo, :celular , :contrasena, :rol, :cedula, :primer_nombre , :segundo_nombre , :primer_apellido , :segundo_apellido , :fecha_registro, :estado)");
        
        $rol = "cliente";
        $estado = "activo";
        $stmt->bindParam(":usuario", $correo, PDO::PARAM_STR);
        $stmt->bindParam(":correo", $correo, PDO::PARAM_STR);
        $stmt->bindParam(":celular", $celular, PDO::PARAM_STR);
        $stmt->bindParam(":contrasena", $contrasena, PDO::PARAM_STR);
        $stmt->bindParam(":rol", $rol, PDO::PARAM_STR);
        $stmt->bindParam(":cedula", $cedula, PDO::PARAM_INT);
        $stmt->bindParam(":primer_nombre", $primer_nombre, PDO::PARAM_STR);
        $stmt->bindParam(":segundo_nombre", $segundo_nombre, PDO::PARAM_STR);
        $stmt->bindParam(":primer_apellido", $primer_apellido, PDO::PARAM_STR);
        $stmt->bindParam(":segundo_apellido", $segundo_apellido, PDO::PARAM_STR);
        $stmt->bindParam(":fecha_registro", $fecha_registro, PDO::PARAM_STR);
        $stmt->bindParam(":estado", $estado, PDO::PARAM_STR);
        

        if($stmt->execute()){
		  	  return true;
        }else{
          return false;
        }
        $stmt->close();
    }



    static public function cargarEstudiantes($item, $value, $orden, $itemOrden){
      if ($item !== null) {
        $conn = Conection::conectar()->prepare("SELECT * FROM usuarios WHERE $item = '$value' ");
        $conn -> execute();
        return $conn->fetchAll();
      }else{
          if ($orden == "ASC") {
              $conn = Conection::conectar()->prepare("SELECT * FROM usuarios ORDER BY $itemOrden ASC ");
              $conn -> execute();
              return $conn->fetchAll();
          }else{
              $conn = Conection::conectar()->prepare("SELECT * FROM usuarios ORDER BY $itemOrden DESC ");
              $conn -> execute();
              return $conn->fetchAll();
          }
      }
    }







    static public function EditarInfoEstudiantesConContrasena($id_usuario, $primer_nombre,$segundo_nombre, $primer_apellido, $segundo_apellido, $cedula, $celular, $correo, $contrasena){
      $stmt = Conection::conectar()->prepare("UPDATE usuarios
      SET usuario = :usuario ,
      correo = :correo ,
      celular = :celular ,
      contrasena = :contrasena ,
      cedula = :cedula ,
      primer_nombre = :primer_nombre ,
      segundo_nombre = :segundo_nombre ,
      primer_apellido = :primer_apellido ,
      segundo_apellido = :segundo_apellido 
      WHERE id = :id_usuario ");

      $stmt->bindParam(":usuario", $correo, PDO::PARAM_STR);    
      $stmt->bindParam(":correo", $correo, PDO::PARAM_STR);      
      $stmt->bindParam(":celular", $celular, PDO::PARAM_STR);
      $stmt->bindParam(":contrasena", $contrasena, PDO::PARAM_STR);
      $stmt->bindParam(":cedula", $cedula, PDO::PARAM_INT);
      $stmt->bindParam(":primer_nombre", $primer_nombre, PDO::PARAM_STR);
      $stmt->bindParam(":segundo_nombre", $segundo_nombre, PDO::PARAM_STR);
      $stmt->bindParam(":primer_apellido", $primer_apellido, PDO::PARAM_STR);
      $stmt->bindParam(":segundo_apellido", $segundo_apellido, PDO::PARAM_STR);
      $stmt->bindParam(":id_usuario", $id_usuario, PDO::PARAM_INT);
      
      if($stmt->execute()){
        return true;
      }else{
        return false;
      }
    }

    
    
    static public function EditarInfoEstudiantesSinContrasena($id_usuario , $primer_nombre,$segundo_nombre, $primer_apellido, $segundo_apellido, $cedula, $celular, $correo){
      
    }










    static public function AsignarCarreraAestudianteDIFERIDO($datos){
      $stmt = Conection::conectar()->prepare("INSERT INTO 
      pagos_diferidos (id_usuario , id_carrera , fecha_registro, estado, valor_total, valor_cuotas, valor_cuota_inicial , n_cuotas , recurrencia_de_pago) 
      VALUES (:id_usuario , :id_carrera , :fecha_registro, :estado, :valor_total, :valor_cuotas, :valor_cuota_inicial , :n_cuotas , :recurrencia_de_pago)");
      $estado = "activo";
      $stmt->bindParam(":id_usuario", $datos["id_usuario"], PDO::PARAM_INT);
      $stmt->bindParam(":id_carrera", $datos["id_carrera"], PDO::PARAM_INT);
      $stmt->bindParam(":fecha_registro", $datos["fecha_registro"], PDO::PARAM_STR);
      $stmt->bindParam(":estado", $estado, PDO::PARAM_STR);
      $stmt->bindParam(":valor_total", $datos["valor_ordinario"], PDO::PARAM_INT);
      $stmt->bindParam(":valor_cuotas", $datos["valor_cuotas"], PDO::PARAM_INT);
      $stmt->bindParam(":valor_cuota_inicial", $datos["cuota_inicial"], PDO::PARAM_INT);
      $stmt->bindParam(":n_cuotas", $datos["numero_cuotas"], PDO::PARAM_INT);
      $stmt->bindParam(":recurrencia_de_pago", $datos["recurrencia"], PDO::PARAM_INT);

      
      if($stmt->execute()){
        return true;
      }else{
        return false;
      }
      $stmt->close();
      
    }

    static public function RegistrarPagoCuotaDiferido($id_diferido , $tipo_cuota, $valor_cuota , $estado , $numero_factura , $fecha_registro){ // para que el usuario estudiante lo vea y proceda al pago 
      $stmt = Conection::conectar()->prepare("INSERT INTO 
      pago_cuotas_diferidos (id_diferido , tipo_cuota, valor_cuota , estado , numero_factura , fecha_registro) 
      VALUES (:id_diferido , :tipo_cuota, :valor_cuota , :estado , :numero_factura , :fecha_registro)");
      $stmt->bindParam(":id_diferido", $id_diferido, PDO::PARAM_INT);
      $stmt->bindParam(":tipo_cuota", $tipo_cuota, PDO::PARAM_STR);
      $stmt->bindParam(":valor_cuota", $valor_cuota, PDO::PARAM_INT);
      $stmt->bindParam(":estado", $estado, PDO::PARAM_STR);
      $stmt->bindParam(":numero_factura", $numero_factura, PDO::PARAM_STR);
      $stmt->bindParam(":fecha_registro", $fecha_registro, PDO::PARAM_STR);

      if($stmt->execute()){
        return true;
      }else{
        return false;
      }
      $stmt->close();

    }

    static public function AsignarCarreraAestudianteDECONTADO($datos){
      $stmt = Conection::conectar()->prepare("INSERT INTO 
      pago_de_contado (fecha_registro , id_usuario , numero_factura, valor_ordinario, estado, id_carrera) 
      VALUES (:fecha_registro , :id_usuario , :numero_factura, :valor_ordinario, :estado, :id_carrera)");
      
      
      $estado = "pendiente";
      $stmt->bindParam(":fecha_registro", $datos["fecha_registro"], PDO::PARAM_STR);
      $stmt->bindParam(":id_usuario", $datos["id_usuario"], PDO::PARAM_INT);
      $stmt->bindParam(":numero_factura", $datos["numero_factura"], PDO::PARAM_STR);
      $stmt->bindParam(":valor_ordinario", $datos["valor_ordinario"], PDO::PARAM_STR);
      $stmt->bindParam(":estado", $estado, PDO::PARAM_STR);
      $stmt->bindParam(":id_carrera", $datos["id_carrera"], PDO::PARAM_INT);
      

      if($stmt->execute()){
        return true;
      }else{
        return false;
      }
      $stmt->close();
    }



    static public function CargarPagosDiferidos($item, $value, $orden, $itemOrden){
      if ($item !== null) {
        $conn = Conection::conectar()->prepare("SELECT * FROM pagos_diferidos WHERE $item = '$value' ORDER BY id DESC ");
        $conn -> execute();
        return $conn->fetchAll();
      }else{
          if ($orden == "ASC") {
              $conn = Conection::conectar()->prepare("SELECT * FROM pagos_diferidos ORDER BY $itemOrden ASC ");
              $conn -> execute();
              return $conn->fetchAll();
          }else{
              $conn = Conection::conectar()->prepare("SELECT * FROM pagos_diferidos ORDER BY $itemOrden DESC ");
              $conn -> execute();
              return $conn->fetchAll();
          }
      }
    }


    static public function CargarPagoCuotasDiferidos($item, $value, $orden, $itemOrden){
      if ($item !== null) {
        $conn = Conection::conectar()->prepare("SELECT * FROM pago_cuotas_diferidos WHERE $item = '$value' ");
        $conn -> execute();
        return $conn->fetchAll();
      }else{
          if ($orden == "ASC") {
              $conn = Conection::conectar()->prepare("SELECT * FROM pago_cuotas_diferidos ORDER BY $itemOrden ASC ");
              $conn -> execute();
              return $conn->fetchAll();
          }else{
              $conn = Conection::conectar()->prepare("SELECT * FROM pago_cuotas_diferidos ORDER BY $itemOrden DESC ");
              $conn -> execute();
              return $conn->fetchAll();
          }
      }
    }


    static public function CargarPagosDeCotando($item, $value, $orden, $itemOrden){
      if ($item !== null) {
        $conn = Conection::conectar()->prepare("SELECT * FROM pago_de_contado WHERE $item = '$value' ");
        $conn -> execute();
        return $conn->fetchAll();
      }else{
          if ($orden == "ASC") {
              $conn = Conection::conectar()->prepare("SELECT * FROM pago_de_contado ORDER BY $itemOrden ASC ");
              $conn -> execute();
              return $conn->fetchAll();
          }else{
              $conn = Conection::conectar()->prepare("SELECT * FROM pago_de_contado ORDER BY $itemOrden DESC ");
              $conn -> execute();
              return $conn->fetchAll();
          }
      }
    }


}