<?php 
require_once"conexion.php";

class MdlCarreras{

    static function RegistrarCarrera($titulo , $precio , $obs , $fecha_registro){
        $stmt = Conection::conectar()->prepare("INSERT INTO 
        carreras (titulo , precio , obs , fecha_registro , estado) 
        VALUES (:titulo , :precio , :obs , :fecha_registro, :estado)");
        $estado = "activa";
        $stmt->bindParam(":titulo", $titulo, PDO::PARAM_STR);
        $stmt->bindParam(":precio", $precio, PDO::PARAM_STR);
        $stmt->bindParam(":obs", $obs, PDO::PARAM_STR);
        $stmt->bindParam(":fecha_registro", $fecha_registro, PDO::PARAM_STR);
        $stmt->bindParam(":estado", $estado, PDO::PARAM_STR);
        

        if($stmt->execute()){
		  	return true;
        }else{
          return false;
        }
        $stmt->close();
    }



    static public function CargarCarreras($item, $value, $orden, $itemOrden)
    {
      if ($item !== null) {
        $conn = Conection::conectar()->prepare("SELECT * FROM  carreras WHERE $item = $value ");
        $conn -> execute();
        return $conn->fetchAll();
      }else{
          if ($orden == "ASC") {
              $conn = Conection::conectar()->prepare("SELECT * FROM carreras ORDER BY $itemOrden ASC ");
              $conn -> execute();
              return $conn->fetchAll();
          }else{
              $conn = Conection::conectar()->prepare("SELECT * FROM carreras ORDER BY $itemOrden DESC ");
              $conn -> execute();
              return $conn->fetchAll();
          }
      }
    }



    static public function EditarCarrera($id_carrera , $titulo , $precio , $obs ){
      $stmt = Conection::conectar()->prepare("UPDATE carreras 
      SET titulo = :titulo ,
      precio = :precio ,
      obs = :obs 
      WHERE id = :id_carrera ");

      $stmt->bindParam(":id_carrera", $id_carrera, PDO::PARAM_INT);
      $stmt->bindParam(":titulo", $titulo, PDO::PARAM_STR);
      $stmt->bindParam(":precio", $precio, PDO::PARAM_STR);
      $stmt->bindParam(":obs", $obs, PDO::PARAM_STR);
      
      if($stmt->execute()){
        return true;
      }else{
        return false;
      }
    }


    static public function CargarDiferidos($id_carrera){
        $conn = Conection::conectar()->prepare("SELECT * FROM pagos_diferidos WHERE id_carrera = $id_carrera ");
        $conn -> execute();
        return $conn->fetchAll();
    }

    static public function CargarPagosDeContado($id_carrera){
      $conn = Conection::conectar()->prepare("SELECT * FROM pagos_de_contado WHERE id_carrera = $id_carrera ");
      $conn -> execute();
      return $conn->fetchAll();
    }


    static public function  EliminarCarrera($id_carrera)
    {
      $stmt = Conection::conectar()->prepare("UPDATE carreras 
      SET estado = :estado 
      WHERE id = :id_carrera ");
      $estado = "eliminada";

      $stmt->bindParam(":id_carrera", $id_carrera, PDO::PARAM_INT);
      $stmt->bindParam(":estado", $estado, PDO::PARAM_STR);
      
      if($stmt->execute()){
        return true;
      }else{
        return false;
      }
    }





}