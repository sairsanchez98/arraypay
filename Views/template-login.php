<!DOCTYPE html>
<!-- 
Template Name: Brunette - Responsive Bootstrap 4 Admin Dashboard Template
Author: Hencework
Contact: https://hencework.ticksy.com/

License: You must have a valid license purchased only from templatemonster to legally use the template for your project.
-->
<html lang="es">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Brunette I Login</title>
    <meta name="description" content="A responsive bootstrap 4 admin dashboard template by hencework" />

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Custom CSS -->
    <link href="Assets/dist/css/style.css" rel="stylesheet" type="text/css">
    
</head>

<body>
    <!-- Preloader -->
    <div class="preloader-it">
        <div class="loader-pendulums"></div>
    </div>
    <!-- /Preloader -->
   
		<!-- HK Wrapper -->
        <div class="hk-wrapper" id="login">
            <!-- Main Content -->
            <div class="hk-pg-wrapper hk-auth-wrapper">
                <header class="d-flex justify-content-between align-items-center">
                    <a class="d-flex auth-brand" href="#">
                        <img width="140px" class="brand-img img-fluid" src="Assets/dist/img/Empresa/logo-empresa.png" alt="brand" />
                    </a>
                    <div class="btn-group btn-group-sm">
                        <a href="#" class="btn btn-outline-secondary">Ayuda</a>
                        <a href="#" class="btn btn-outline-secondary">Acerca de</a>
                    </div>
                </header>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-5 pa-0">
                            <div id="owl_demo_1" class="owl-carousel dots-on-item owl-theme">
                                <div class="fadeOut item auth-cover-img overlay-wrap" style="background-image:url(Assets/dist/img/Empresa/Unicor-bg1.jpg);">
                                    <div class="auth-cover-info py-xl-0 pt-100 pb-50">
                                        <div class="auth-cover-content text-center w-xxl-75 w-sm-90 w-xs-100">
                                            <h2 class="display-5 text-white mb-20">Realizar tus pagos nunca fue tan fácil.</h2>
                                            <p class="text-white">The purpose of lorem ipsum is to create a natural looking block of text (sentence, paragraph, page, etc.) that doesn't distract from the layout. Again during the 90s as desktop publishers bundled the text with their software.</p>
                                        </div>
                                    </div>
                                    <div class="bg-overlay bg-trans-dark-50"></div>
                                </div>
                                <div class="fadeOut item auth-cover-img overlay-wrap" style="background-image:url(Assets/dist/img/Empresa/Unicor-bg2.jpg);">
                                    <div class="auth-cover-info py-xl-0 pt-100 pb-50">
                                        <div class="auth-cover-content text-center w-xxl-75 w-sm-90 w-xs-100">
                                            <h2 class="display-5 text-white mb-20">Realiza tus pagos universitarios desde donde estés.</h2>
                                            <p class="text-white">The passage experienced a surge in popularity during the 1960s when Letraset used it on their dry-transfer sheets, and again during the 90s as desktop publishers bundled the text with their software.</p>
                                        </div>
                                    </div>
                                    <div class="bg-overlay bg-trans-dark-50"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-7 pa-0">
                            <div class="auth-form-wrap py-xl-0 py-50">
                                <div class="auth-form w-xxl-55 w-xl-75 w-sm-90 w-xs-100">
                                    <form>
                                        <h3 class="display-6 mb-10">Bienvenido al sistema de pagos en línea #ArrayPay</h3>
                                        <p class="mb-30">Para ingresar usa tu usuario y contraseña </p>

                                        
                                        <div v-if="alert" class="alert alert-warning alert-wth-icon alert-dismissible fade show" role="alert">
                                            <span class="alert-icon-wrap"><i class="zmdi zmdi-alert-circle"></i></span>
                                            <p v-text="alert"></p>
                                            <!-- <a href="#" class="alert-link mt-5">Follow</a> -->
                                        </div>



                                        <div class="form-group">
                                            <input v-model="user" class="form-control"  type="email">
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input v-model="pass" class="form-control"  type="password">
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><span class="feather-icon"><i data-feather="eye-off"></i></span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="custom-control custom-checkbox mb-25">
                                            <input class="custom-control-input" id="same-address" type="checkbox" checked>
                                            <label class="custom-control-label font-14" for="same-address">Recuerdame</label>
                                        </div>
                                        <button v-if="!procesando" @click="Login()" class="btn btn-success btn-block" type="button">Acceso</button>
                                        <button v-if="procesando" class="btn btn-success btn-block" type="button">Cargando...</button>
                                        
                                        <a href="#"><p class="font-14 text-center mt-15">Ir a PowerCampus</p></a>
                                        <div class="text-center text-dark mb-10">¡Más medios de pago, más posibilidades!</div>

                                        <div class="form-row">
                                            <div class="col-sm-12 mb-20">
                                                <!-- <button class="btn btn-indigo btn-block btn-wth-icon"> <span class="icon-label"><i class="fa fa-facebook"></i> </span><span class="btn-text">Login with facebook</span></button> -->
                                                <!-- <img class="img align-img-center" src="Assets/dist/img/Unicor/medios_de_pago.png">  -->
                                                <div class="w-100 bg-light">
                                                    <img src="Assets/dist/img/Empresa/medios_de_pago.png" class="img-fluid mx-auto d-block" alt="img">
                                                </div>
                                            </div>
                                            <!-- <div class="col-sm-6 mb-20">
                                                <button class="btn btn-sky btn-block btn-wth-icon"> <span class="icon-label"><i class="fa fa-twitter"></i> </span><span class="btn-text">Login with Twitter</span></button>
                                            </div> -->
                                        </div>
                                        <!-- <p class="text-center">Do have an account yet? <a href="#">Sign Up</a></p> -->
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Main Content -->  
        </div>
        <!-- /HK Wrapper -->

    <!-- jQuery -->
    <script src="Assets/vue.js"></script>
    <script src="Assets/axios.min.js"></script>
    <script src="Controllers/JS/CtrlLogin.js"></script>

    <script src="Assets/vendors/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="Assets/vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="Assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Slimscroll JavaScript -->
    <script src="Assets/dist/js/jquery.slimscroll.js"></script>

    <!-- Fancy Dropdown JS -->
    <script src="Assets/dist/js/dropdown-bootstrap-extended.js"></script>

    <!-- Owl JavaScript -->
    <script src="Assets/vendors/owl.carousel/dist/owl.carousel.min.js"></script>

    <!-- FeatherIcons JavaScript -->
    <script src="Assets/dist/js/feather.min.js"></script>

    <!-- Init JavaScript -->
    <script src="Assets/dist/js/init.js"></script>
    <script src="Assets/dist/js/login-data.js"></script>


    

</body>

</html>