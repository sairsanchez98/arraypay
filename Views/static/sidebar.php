 <!-- partial:partials/_sidebar.html -->
 <nav class="sidebar sidebar-offcanvas" >
          <ul class="nav">
            <!-- <li class="nav-item nav-profile">
              <a href="#" class="nav-link">
                <div class="nav-profile-image">
                  <img src="assets/images/faces/face1.jpg" alt="profile">
                  <span class="login-status online"></span>
                  
                </div>
                <div class="nav-profile-text d-flex flex-column">
                  <span class="font-weight-bold mb-2">David Grey. H</span>
                  <span class="text-secondary text-small">Project Manager</span>
                </div>
                <i class="mdi mdi-bookmark-check text-success nav-profile-badge"></i>
              </a>
            </li>  -->
            <br><br><br><br><br>
            <li class="nav-item">
              <a class="nav-link" href="./">
                <span class="menu-title">Resumen</span>
                <i class="mdi mdi-view-dashboard menu-icon"></i>
              </a>
            </li>
            <!-- <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                <span class="menu-title">Basic UI Elements</span>
                <i class="menu-arrow"></i>
                <i class="mdi mdi-crosshairs-gps menu-icon"></i>
              </a>
              <div class="collapse" id="ui-basic">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item"> <a class="nav-link" href="pages/ui-features/buttons.html">Buttons</a></li>
                  <li class="nav-item"> <a class="nav-link" href="pages/ui-features/typography.html">Typography</a></li>
                </ul>
              </div>
            </li> -->
            <li class="nav-item">
              <a class="nav-link" href="pages/icons/mdi.html">
                <span class="menu-title">Pedidos</span>
                <i class="mdi mdi-folder-account menu-icon"></i>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="pages/forms/basic_elements.html">
                <span class="menu-title">Servicios</span>
                <i class="mdi mdi-briefcase-check menu-icon"></i>
                
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="pages/charts/chartjs.html">
                <span class="menu-title">Facturación</span>
                <i class="mdi mdi-clipboard-text menu-icon"></i>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="pages/tables/basic-table.html">
                <span class="menu-title">Centro de ayuda</span>
                <i class="mdi mdi-help-circle menu-icon"></i>
              </a>
            </li>
            <?php if($_SESSION["UserLoggedIn"]["user"]["rol"] == "admin"): ?>
            <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#general-pages" aria-expanded="false" aria-controls="general-pages">
                <span class="menu-title">Informes</span>
                <i class="menu-arrow"></i>
                <i class="mdi mdi-library-books menu-icon"></i>
              </a>
              <div class="collapse" id="general-pages">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item"> <a class="nav-link" href="pages/samples/blank-page.html"> Pedidos </a></li>
                  <li class="nav-item"> <a class="nav-link" href="pages/samples/login.html"> Pagos </a></li>
                  <li class="nav-item"> <a class="nav-link" href="pages/samples/register.html"> Clientes </a></li>
                </ul>
              </div>
            </li>
          <?php endif; ?>
          
          </ul>
        </nav>