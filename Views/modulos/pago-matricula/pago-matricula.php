<div class="hk-pg-wrapper" id="pago_matriculas">
			<!-- Container -->
            <div class="container mt-xl-50 mt-sm-30 mt-15">
                <!-- Title -->
                <div class="hk-pg-header">
                    <div>
						<h3 class="hk-pg-title font-weight-600 mb-10">Pago de matrícula</h3>
						<p>¡Pagar tu mátricula es muy facíl y lo puedes hacer desde cualquier lugar!
								<i class="ion ion-md-help-circle-outline ml-5" 
								data-toggle="tooltip" data-placement="top" 
								title="Ver video tutorial">
								</i>
						</p>
                    </div>
					<!-- <div class="d-flex">
                        <div class="btn-group btn-group-sm" role="group">
							<button type="button" class="btn btn-outline-secondary active">today</button>
							<button type="button" class="btn btn-outline-secondary">week</button>
							<button type="button" class="btn btn-outline-secondary">month</button>
							<button type="button" class="btn btn-outline-secondary">quarter</button>
							<button type="button" class="btn btn-outline-secondary">year</button>
						</div>
                    </div> -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                <div class="row">
                    <div class="col-xl-12">
						<!-- Page Alerts -->
						<div v-if="alert" class="alert alert-warning alert-wth-icon alert-dismissible fade show" role="alert">
							<span class="alert-icon-wrap"><i class="fa fa-exclamation-triangle"></i></span> 
							<p v-text="alert"></p>
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
						</div> 
						<!-- /Page Alerts -->

						
						<section class="hk-sec-wrapper">
                            <!-- <h5 class="hk-sec-title">Data Table</h5>
                            <p class="mb-40">Add advanced interaction controls to HTML tables like <code>search, pagination & selectors</code>. For responsive table just add the <code>responsive: true</code> to your DataTables function. <a href="https://datatables.net/reference/option/" target="_blank">View all options</a>.</p> -->
                            <div class="row">
                                <div class="col-sm">
									<div class="table-wrap">
										<div class="table-responsive">
										
												<table class="table table-lg mb-0">
											
												<thead class="thead-success">
													<tr>
														<th>Periodo académico</th>
														<th>Carrera</th>
														<th>Pago ordinario</th>
														<th>Pago extraordinario</th>
														<th>Estado</th>
														<th>Opciones</th>
													</tr>
												</thead>
												<tbody>
													<tr v-for="(pm, index) in pago_matriculas_DB">
														<td>{{pm.periodo}}</td>
														<td>{{pm.carrera}}</td>
														<td >
															${{pm.valor_ordinario}} <br><footer class="blockquote-footer">(pagar hasta: {{pm.fecha_pago_ordinario}})</footer>
														</td>
														<td >
															${{pm.valor_extraordinario}}<br><footer class="blockquote-footer">(pagar hasta: {{pm.fecha_pago_extraordinario}})</footer>
														</td>
														<td>
                                                            <div v-if="pm.estado == 'pagado'" class="badge badge-warning">Pagado</div>
															<div v-else="pm.estado == 'pendiente'" class="badge badge-danger">Pendiente</div>
                                                        </td>
														<td>
															
															<button type="button" class="mr-25 btn btn-info "@click="Pay(index)"
																data-toggle="tooltip" data-original-title="Pagar en línea"> 
																<i class="fa fa-file-invoice-dollar"></i> 
															</button>
															<button type="button" class="btn btn-info" data-toggle="tooltip" 
																data-original-title="Descargar volante"> 
															    <i class="fa fa-file-download "></i> 
															</button>
															
														</td>
													</tr>
												</tbody>
											
											</table>
										</div>
									</div>
                                </div>
                            </div>
                        </section>
						
						
					</div>
                </div>
                <!-- /Row -->
			</div>
            <!-- /Container -->
			
</div>
<script type="text/javascript" src="https://checkout.epayco.co/checkout.js"></script>
<script src="Controllers/JS/CtrlPagoMatricula.js"></script>



	


		