<div class="hk-pg-wrapper" id="pago_matriculas">
			<!-- Container -->
            <div class="container mt-xl-50 mt-sm-30 mt-15">
                <!-- Row -->
                <div class="row">
                  <div class="col-xl-12">
                    <!-- Page Alerts -->
                    <div id="alert_" class="alert alert-warning alert-wth-icon alert-dismissible fade show" role="alert">
                      <span class="alert-icon-wrap"><i id="icon_alert" class="fa fa-exclamation-triangle"></i></span> 
                        <p id="alert"></p>
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div> 
                    <!-- /Page Alerts -->
                    <section class="hk-sec-wrapper">
                        <div class="row">
                            <div class="col-sm">
                              <header id="main-header" style="margin-top:20px">
                                <div class="row">
                                  <div class="col-lg-12 franja">
                                    <img class="center-block" src="Assets/dist/img/Unicor/unicordoba_logo1.png" style="">
                                  </div>
                                </div>
                              </header>
                              <div class="container">
                                <div class="row" style="margin-top:20px">
                                  <div class="col-lg-8 col-lg-offset-2 ">
                                    <h4 style="text-align:left"> Respuesta de la Transacción </h4>
                                    <hr>
                                  </div>
                                  <div class="col-lg-8 col-lg-offset-2 ">
                                    <div class="table-responsive">
                                      <table class="table table-bordered">
                                        <tbody>
                                          <tr>
                                            <td>Referencia</td>
                                            <td id="referencia"></td>
                                          </tr>
                                          <tr>
                                            <td class="bold">Fecha</td>
                                            <td id="fecha" class=""></td>
                                          </tr>
                                          <tr>
                                            <td>Respuesta</td>
                                            <td id="respuesta"></td>
                                          </tr>
                                          <tr>
                                            <td>Motivo</td>
                                            <td id="motivo"></td>
                                          </tr>
                                          <tr>
                                            <td class="bold">Banco</td>
                                            <td class="" id="banco">
                                          </tr>
                                          <tr>
                                            <td class="bold">Recibo</td>
                                            <td id="recibo"></td>
                                          </tr>
                                          <tr>
                                            <td class="bold">Total</td>
                                            <td class="" id="total">
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                                <footer>
                                  <div class="row">
                                    <div class="container">
                                      <div class="col-lg-8 col-lg-offset-2">
                                        <img src="https://369969691f476073508a-60bf0867add971908d4f26a64519c2aa.ssl.cf5.rackcdn.com/btns/epayco/pagos_procesados_por_epayco_260px.png" style="margin-top:10px; float:left"> <img src="https://369969691f476073508a-60bf0867add971908d4f26a64519c2aa.ssl.cf5.rackcdn.com/btns/epayco/credibancologo.png"
                                          height="40px" style="margin-top:10px; float:right">
                                          <img class="mr-10" src="https://array.com.co/wp-content/uploads/2019/06/ARRAYTICW-1.png"
                                          height="40px" style="margin-top:10px; float:right">
                                          
                                      </div>
                                    </div>
                                  </div>
                                </footer>
                            </div>
                        </div>
                    </section>
                  </div>
                </div>
                <!-- /Row -->
			      </div>
            <!-- /Container -->
		
</div>




  
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script>
    function getQueryParam(param) {
      location.search.substr(1)
        .split("&")
        .some(function(item) { // returns first occurence and stops
          return item.split("=")[0] == param && (param = item.split("=")[1])
        })
      return param
    }
    $(document).ready(function() {
      //llave publica del comercio

      //Referencia de payco que viene por url
      var ref_payco = getQueryParam('ref_payco');
      //Url Rest Metodo get, se pasa la llave y la ref_payco como paremetro
      var urlapp = "https://secure.epayco.co/validation/v1/reference/" + ref_payco;

      $.get(urlapp, function(response) {


        if (response.success) {

          if (response.data.x_cod_response == 1) {
            //Codigo personalizado
            
            $("#alert_").attr("class","alert alert-success alert-wth-icon alert-dismissible fade show");
            $("#icon_alert").attr("class","fa fa-check");
            $("#alert").text("Transacción aprobada");
            //console.log('transacción aceptada');
          }
          //Transaccion Rechazada
          if (response.data.x_cod_response == 2) {
            $("#alert_").attr("class","alert alert-danger alert-wth-icon alert-dismissible fade show");
            $("#icon_alert").attr("class","fa fa-exclamation-triangle");
            $("#alert").text("Transacción rechazada, intente realizar el pago nuevamente");
          }
          //Transaccion Pendiente
          if (response.data.x_cod_response == 3) {
            $("#alert_").attr("class","alert alert-info alert-wth-icon alert-dismissible fade show");
            $("#icon_alert").attr("class","fa fa-tasks");
            $("#alert").text("Transacción pendiente de pago");
          }
          //Transaccion Fallida
          if (response.data.x_cod_response == 4) {
            $("#alert_").attr("class","alert alert-danger alert-wth-icon alert-dismissible fade show");
            $("#icon_alert").attr("class","fa fa-exclamation-triangle");
            $("#alert").text("Transacción fallida, intente realizar el pago nuevamente");
          }

          $('#fecha').html(response.data.x_transaction_date);
          $('#respuesta').html(response.data.x_response);
          $('#referencia').text(response.data.x_id_invoice);
          $('#motivo').text(response.data.x_response_reason_text);
          $('#recibo').text(response.data.x_transaction_id);
          $('#banco').text(response.data.x_bank_name);
          $('#autorizacion').text(response.data.x_approval_code);
          $('#total').text(response.data.x_amount + ' ' + response.data.x_currency_code);


        } else {
          $("#alert_").attr("class","alert alert-danger alert-wth-icon alert-dismissible fade show");
            $("#icon_alert").attr("class","fa fa-exclamation-triangle");
            $("#alert").text("Error consultando la información");
        }
      });

    });
  </script>


