
<div class="hk-pg-wrapper" id="gestion-estudiantes">
			<!-- Container -->
            <div class="container mt-xl-50 mt-sm-30 mt-15">
                <!-- Title -->
                <div class="hk-pg-header">
                    <div>
						<h3 class="hk-pg-title font-weight-600 mb-10">Gestión de estudiantes</h3>
                    </div>
					<div class="d-flex">
                        <div class="btn-group btn-group-sm" role="group">
							<button @click="HabilitarComponente('listado') , CargarEstudiantes()" type="button" class="btn btn-outline-info active">Listado</button>
							<button @click="HabilitarComponente('registrar')" type="button" class="btn btn-outline-info">Registrar</button>
						</div>
                    </div>
                </div>
                <!-- /Title -->

                <!-- Row -->
                <div class="row">
                    <div class="col-xl-12">
						<!-- Page Alerts -->
						<div v-if="alert" :class="['alert alert-wth-icon alert-dismissible fade show' , alert_class]" role="alert">
							<span class="alert-icon-wrap"><i :class="[alert_icon]"></i></span> 
							<p v-text="alert"></p>
							<button type="button" class="close" @click="alert = false">
								<span aria-hidden="true">×</span>
							</button>
						</div> 
						<!-- /Page Alerts -->
						
						<section class="hk-sec-wrapper">
                            <!-- <h5 class="hk-sec-title">Data Table</h5>
                            <p class="mb-40">Add advanced interaction controls to HTML tables like <code>search, pagination & selectors</code>. For responsive table just add the <code>responsive: true</code> to your DataTables function. <a href="https://datatables.net/reference/option/" target="_blank">View all options</a>.</p> -->
                            <div class="row">
                                <div class="col-sm">
									
                                    <!-- TABLA LISTADO DE CARRERAS  -->
                                    <div v-for="comp in componentes" v-show="comp.comp=='listado' && comp.estado==true" class="table-wrap">
											<table class="table table-lg mb-0 " id="datable_1">
												<thead class="thead-success">
													<tr>
                                                        <th>CC</th>
                                                        <th>Nombre</th>
                                                        <th>Contacto</th>
														<th>Opciones</th>
													</tr>
												</thead>
												<tbody>
												</tbody>
											</table>
									</div>
                                    <!--END TABLA LISTADO DE CARRERAS  -->



                                    <!-- FORMULARIO DE REGISTRO DE  NUEVOS ESTUDIANTES-->
                                    <form class="mb-30" v-for="comp in componentes" v-if="comp.comp=='registrar' && comp.estado==true">
                                        <div class="row">
                                            <div class="col-md-3 form-group">
                                                <label for="username">Primer nombre:</label>
                                                <div class="input-group">
                                                    <input v-model="primer_nombre" :class="['form-control', input_primer_nombre]" type="text">
                                                </div>
                                            </div> 
                                            <div class="col-md-3 form-group">
                                                <label for="username">Segundo nombre:</label>
                                                <div class="input-group">
                                                    <input v-model="segundo_nombre" :class="['form-control', input_segundo_nombre]" type="text">
                                                </div>
                                            </div> 
                                            <div class="col-md-3 form-group">
                                                <label for="username">Primer apellido:</label>
                                                <div class="input-group">
                                                    <input v-model="primer_apellido" :class="['form-control', input_primer_apellido]" type="text">
                                                </div>
                                            </div> 
                                            <div class="col-md-3 form-group">
                                                <label for="username">Segundo apellido:</label>
                                                <div class="input-group">
                                                    <input v-model="segundo_apellido" :class="['form-control', input_segundo_apellido]" type="text">
                                                </div>
                                            </div> 

                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 form-group">
                                                <label for="username">Cédula:</label>
                                                <div class="input-group">
                                                    <input v-model="cedula" :class="['form-control', input_cedula]"  type="number">
                                                </div>
                                            </div>
                                            <div class="col-md-3 form-group">
                                                <label >Celular:</label>
                                                <div class="input-group">
                                                    <input v-model="celular" :class="['form-control', input_celular]"  type="number">
                                                </div>
                                            </div>
                                            <div class="col-md-3 form-group">
                                                <label >Correo electrónico:</label>
                                                <div class="input-group">
                                                    <input v-model="correo" :class="['form-control', input_correo]"  type="email">
                                                </div>
                                            </div>
                                            <div class="col-md-3 form-group">
                                                <label >Contraseña:</label>
                                                <div class="input-group">
                                                    <input v-model="contrasena" :class="['form-control', input_contrasena]"  type="password">
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <button v-if="!registro_procesando" type="button" class="btn btn-success mt-10" @click="RegistrarEstudiante()" >Registrar</button>
                                        <button v-if="registro_procesando" type="button" class="btn btn-success mt-10" >Procesando...</button>
                                    </form>
                                    <!-- FORMULARIO DE REGISTRO DE  NUEVOS ESTUDIANTES-->


                                    <!-- EDITAR USUARIO  -->
                                    <div v-show="editar_ver_info_est">
                                        <h4 class = "mt-10 mb-10" >Editar información estudiante: {{estudiante_seleccionado[1]}} | {{estudiante_seleccionado[2]}}</h4>
                                        <form class="mb-30" autocomplete="off" >
                                            <div class="row">
                                                <div class="col-md-3 form-group">
                                                    <label for="username">Primer nombre:</label>
                                                    <div class="input-group">
                                                        <input v-model="estudiante_seleccionado[3]" :class="['form-control', input_primer_nombre]" type="text">
                                                    </div>
                                                </div> 
                                                <div class="col-md-3 form-group">
                                                    <label for="username">Segundo nombre:</label>
                                                    <div class="input-group">
                                                        <input v-model="estudiante_seleccionado[4]" :class="['form-control', input_segundo_nombre]" type="text">
                                                    </div>
                                                </div> 
                                                <div class="col-md-3 form-group">
                                                    <label for="username">Primer apellido:</label>
                                                    <div class="input-group">
                                                        <input v-model="estudiante_seleccionado[5]" :class="['form-control', input_primer_apellido]" type="text">
                                                    </div>
                                                </div> 
                                                <div class="col-md-3 form-group">
                                                    <label for="username">Segundo apellido:</label>
                                                    <div class="input-group">
                                                        <input v-model="estudiante_seleccionado[6]" :class="['form-control', input_segundo_apellido]" type="text">
                                                    </div>
                                                </div> 

                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 form-group">
                                                    <label for="username">Cédula:</label>
                                                    <div class="input-group">
                                                        <input v-model="estudiante_seleccionado[1]" :class="['form-control', input_cedula]"  type="number">
                                                    </div>
                                                </div>
                                                <div class="col-md-3 form-group">
                                                    <label >Celular:</label>
                                                    <div class="input-group">
                                                        <input v-model="estudiante_seleccionado[9]" :class="['form-control', input_celular]"  type="number">
                                                    </div>
                                                </div>
                                                <div class="col-md-3 form-group">
                                                    <label >Correo electrónico:</label>
                                                    <div class="input-group">
                                                        <input v-model="estudiante_seleccionado[10]" :class="['form-control', input_correo]"  type="email">
                                                    </div>
                                                </div>
                                                <div class="col-md-3 form-group">
                                                    <label >Nueva Contraseña:</label>
                                                    <div class="input-group">
                                                        <input autocomplete="off"  v-model="nueva_contrasena" :class="['form-control', input_contrasena]"  type="password">
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <button v-if="!editar_procesando" type="button" class="btn btn-success mt-10" @click="EditarEstudiante()" >Guardar cambios</button>
                                            <button v-if="editar_procesando" type="button" class="btn btn-success mt-10" >Procesando...</button>
                                        </form>

                                        

                                        <hr>
                                        <h4 class = "mt-10 mb-10" >Asignar carrera</h4>
                                            <!-- Page Alerts -->
                                                <div v-if="alert_asignar_carrera" :class="['alert alert-wth-icon alert-dismissible fade show' , alert_class]" role="alert">
                                                    <span class="alert-icon-wrap"><i :class="[alert_icon]"></i></span> 
                                                    <p v-text="alert_asignar_carrera"></p>
                                                    <button type="button" class="close" @click="alert = false">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div> 
                                            <!-- /Page Alerts -->
                                        <form class="mb-30" >
                                            <div class="row">
                                                <div class="col-md-12 form-group">
                                                    <label >Carrera:</label>
                                                    <select :class="['form-control select_carreras']" id="carreras" >
                                                        <option value= ""></option>
                                                        <option  v-for="(carrera , index) in CarrerasDB" :value="index">
                                                            {{carrera.titulo}}
                                                        </option>
                                                    </select>
                                                </div>
                                            </div> 
                                            <div class="row">
                                                <div class="col-md-3 form-group">
                                                    <label for="username">Total:</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">$</span>
                                                        </div>
                                                        <input disabled style="background: white; color: black;"  v-model="valor_total" 
                                                        :class="['form-control', input_cedula]"  type="number">
                                                    </div>
                                                </div> 
                                                <div class="col-md-3 form-group">
                                                    <label >Tipo de pago:</label>
                                                    <select v-model="tipo_de_pago" class="form-control" >
                                                        <option value= ""></option>
                                                        <option value= "decontado">Pago de contado</option>
                                                        <option value= "diferido">Pago diferido </option>
                                                        
                                                    </select>
                                                </div>
                                                <div class="col-md-3 form-group">
                                                    <label for="username">Total personalizado:</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">$</span>
                                                        </div>
                                                        <input @keyup="calcularValoresdePago('total_personalizado')" v-model="total_personalizado" 
                                                        :class="['form-control']" type="number" min="1">
                                                    </div>
                                                </div> 
                                                
                                                <div v-if= "tipo_de_pago == 'diferido'" class="col-md-3 form-group">
                                                    <label for="username">Número de Cuotas:</label>
                                                    <div class="input-group">
                                                        <input  @keyup="calcularValoresdePago('numero_cuotas')" v-model="numero_cuotas" 
                                                        :class="['form-control']" type="number" min="2" max="24">
                                                    </div>
                                                </div> 
                                               
                                            </div>
                                            <div class="row">
                                                <div v-if= "tipo_de_pago == 'diferido'" class="col-md-3 form-group">
                                                    <label for="username">Cuota inicial personalizada:</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">$</span>
                                                        </div>
                                                        <input @keyup="calcularValoresdePago('cuota_inicial_personalizada')" v-model="cuota_inicial_personalizada" 
                                                        :class="['form-control']" type="number" min="1">
                                                    </div>
                                                </div> 

                                                <div v-if= "tipo_de_pago == 'diferido'" class= "col-md-3 form-group">
                                                    <label for="username">Cuota inicial fija:</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">$</span>
                                                        </div>
                                                        <input @click="calcularValoresdePago('cuota_inicial')" @keyup="calcularValoresdePago('cuota_inicial')" 
                                                        v-model="cuota_inicial" :class="['form-control', input_segundo_apellido]" type="number">
                                                    </div>
                                                </div> 
                                                <div v-if= "tipo_de_pago == 'diferido'" class="col-md-3 form-group">
                                                    <label for="username">Valor cuotas:</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">$</span>
                                                        </div>
                                                        <input disabled style="background: white; color: black;"  v-model="valor_cuotas" 
                                                        :class="['form-control', input_cedula]"  type="number">
                                                    </div>
                                                </div>
                                                <div v-if= "tipo_de_pago == 'diferido'" class="col-md-3 form-group">
                                                    <label for="username">Recurrencia:</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">Cada</span>
                                                        </div>
                                                        <input v-model="recurrencia" :class="['form-control', input_cedula]"  type="number">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">días</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button v-if="!asignacion_procesando" type="button" class="btn btn-success mt-10" @click="AsignarCarreraAestudiante()" >Asignar</button>
                                            <button v-if="asignacion_procesando" type="button" class="btn btn-success mt-10" >Procesando...</button>
                                        </form>


                                        <hr>
                                        <h4 class = "mt-10 mb-10" >Carreras asignadas (de contado)</h4>

                                        <table class="table table-lg mb-0 " id="datable_1">
                                            <thead class="thead-success">
                                                <tr>
                                                    <th>Carrera</th>
                                                    <th>Asignación</th>
                                                    <th>Estado</th>
                                                    <th>Opciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
										</table>

                                        <hr>
                                        <h4 class = "mt-10 mb-10" >Carreras asignadas (diferido)</h4>

                                        <table class="table table-lg mb-0 " id="datable_1">
                                            <thead class="thead-success">
                                                <tr>
                                                    <th>Carrera</th>
                                                    <th>Asignación</th>
                                                    <th>Estado</th>
                                                    <th>N. cuotas</th>
                                                    <th>N. cuotas pagadas</th>
                                                    <th>Cuotas atrasadas</th>
                                                    <th>Opciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
										</table>
                                    </div>
                                    <!-- EDITAR USUARIO  -->



                                </div>
                            </div>
                        </section>
						
						
					</div>
                </div>
                <!-- /Row -->
			</div>
            <!-- /Container -->
			
</div>

     <!-- Select2 JavaScript -->
     <script src="Assets/vendors/select2/dist/js/select2.full.min.js"></script>

    
    <!-- Data Table JavaScript -->
    <script src="Assets/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="Assets/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="Assets/vendors/datatables.net-dt/js/dataTables.dataTables.min.js"></script>
    <script src="Assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="Assets/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="Assets/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="Assets/vendors/jszip/dist/jszip.min.js"></script>
    <script src="Assets/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="Assets/vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="Assets/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="Assets/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="Assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <!-- <script src="Assets/dist/js/dataTables-data.js"></script> -->

    
    
    
    <script src="Controllers/JS/CtrlGestionEstudiantes.js"></script>	

    


