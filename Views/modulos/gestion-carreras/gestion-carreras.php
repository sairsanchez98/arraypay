<div class="hk-pg-wrapper" id="gestion-carreras">
			<!-- Container -->
            <div class="container mt-xl-50 mt-sm-30 mt-15">
                <!-- Title -->
                <div class="hk-pg-header">
                    <div>
						<h3 class="hk-pg-title font-weight-600 mb-10">Gestión de carreras</h3>
                    </div>
					<div class="d-flex">
                        <div class="btn-group btn-group-sm" role="group">
							<button @click="HabilitarComponente('listado') , CargarCarreras()" type="button" class="btn btn-outline-info active">Listado</button>
							<button @click="HabilitarComponente('registrar')" type="button" class="btn btn-outline-info">Registrar</button>
						</div>
                    </div>
                </div>
                <!-- /Title -->

                <!-- Row -->
                <div class="row">
                    <div class="col-xl-12">
						<!-- Page Alerts -->
						<div v-if="alert" :class="['alert alert-wth-icon alert-dismissible fade show' , alert_class]" role="alert">
							<span class="alert-icon-wrap"><i :class="[alert_icon]"></i></span> 
							<p v-text="alert"></p>
							<button type="button" class="close" @click="alert = false">
								<span aria-hidden="true">×</span>
							</button>
						</div> 
						<!-- /Page Alerts -->

						
						<section class="hk-sec-wrapper">
                            <!-- <h5 class="hk-sec-title">Data Table</h5>
                            <p class="mb-40">Add advanced interaction controls to HTML tables like <code>search, pagination & selectors</code>. For responsive table just add the <code>responsive: true</code> to your DataTables function. <a href="https://datatables.net/reference/option/" target="_blank">View all options</a>.</p> -->
                            <div class="row">
                                <div class="col-md">
									
                                    <!-- TABLA LISTADO DE CARRERAS  -->
                                    <div v-for="comp in componentes" v-show="comp.comp=='listado' && comp.estado==true" class="table-wrap">
										
										
                                        <table class="table  row-border" id="datable_1">
                                        
                                            <thead class="thead-success">
                                                <tr>
                                                    <th>Titulo</th>
                                                    <th>Obs</th>
                                                    <th>Valor</th>
                                                    <th>N. Inscritos</th>
                                                    <th>Opciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        
                                        </table>
										
									</div>
                                    <!--END TABLA LISTADO DE CARRERAS  -->



                                    <!-- FORMULARIO DE REGISTRO DE NUEVAS CARRERAS -->
                                    <form class="mb-30" v-for="comp in componentes" v-if="comp.comp=='registrar' && comp.estado==true">
                                        <div class="form-group">
                                            <label for="inputPassword5">Titulo:</label>
                                            <input v-model="titulo" type="text" id="inputPassword5" :class="['form-control', alert_input_titulo]" aria-describedby="passwordHelpBlock">
                                            <!-- <small id="passwordHelpBlock" class="form-text text-muted">
												  Your password must be 8-20 characters long, contain letters and numbers, and must not contain spaces, special characters, or emoji.
											</small> -->

                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 form-group">
                                                <label for="username">Precio:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fa fa-dollar-sign"></i></span>
                                                    </div>
                                                    <input v-model="precio" :class="['form-control' , alert_input_precio]" id="username" placeholder="Ej: 200000" type="number">
                                                </div>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <label for="lastName">Observación:</label>
                                                <input v-model="obs" :class="['form-control' , alert_input_obs]" id="lastName" placeholder="" value="" type="text">
                                            </div>

                                            

                                        </div>
                                        <button 
                                            @click="RegistrarCarrera()" 
                                            class="btn btn-success mt-10" 
                                            type="button">{{textbutton}}
                                        </button>
                                      
                                    </form>
                                    <!-- FORMULARIO DE REGISTRO DE NUEVAS CARRERAS -->



                                     <!-- FORMULARIO DE EDITAR CARRERAS -->
                                     <form class="mb-30" v-for="comp in componentes" v-if="comp.comp=='editarCarrera' && comp.estado==true">
                                        <div class="form-group">
                                            <label for="inputPassword5">Titulo:</label>
                                            <input v-model="edit_titulo" type="text" id="inputPassword5" :class="['form-control', alert_input_titulo]" aria-describedby="passwordHelpBlock">
                                            <!-- <small id="passwordHelpBlock" class="form-text text-muted">
												  Your password must be 8-20 characters long, contain letters and numbers, and must not contain spaces, special characters, or emoji.
											</small> -->

                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 form-group">
                                                <label for="username">Precio:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fa fa-dollar-sign"></i></span>
                                                    </div>
                                                    <input v-model="edit_precio" :class="['form-control' , alert_input_precio]" id="username" placeholder="Ej: 200000" type="number">
                                                </div>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <label for="lastName">Observación:</label>
                                                <input v-model="edit_obs" :class="['form-control' , alert_input_obs]" id="lastName" placeholder="" value="" type="text">
                                            </div>

                                            

                                        </div>
                                        <button 
                                            @click="EditarCarrera()" 
                                            class="btn btn-success mt-10" 
                                            type="button">{{textbuttonEdit}}
                                        </button>
                                      
                                    </form>
                                    <!-- FORMULARIO DE EDITAR CARRERAS -->


                                    
                                    
                                    <!-- ELIMINAR CARRERA -->
                                    <div class="col-sm-12" v-for="comp in componentes" v-if="comp.comp=='eliminarCarrera' && comp.estado==true">
                                        <div class="alert alert-warning alert-wth-icon alert-dismissible fade show" role="alert">
                                            <span class="alert-icon-wrap"><i class="zmdi zmdi-alert-circle-o"></i></span>
                                            <h5 class="alert-heading">¿Desea eliminar la carrera?</h5>
                                            <p>Carrera: <b>{{edit_titulo}}</b></p>
                                            <button class="btn btn-secondary mt-20 mr-5" @click="HabilitarComponente('listado')">Cancelar</button>
                                            <button class="btn btn-danger mt-20" @click="EliminarCarrera()">Si, eliminar</button>
                                        </div>
                                    </div>
                                    <!-- ELIMINAR CARRERA -->



                                </div>
                            </div>
                        </section>
						
						
					</div>
                </div>
                <!-- /Row -->
			</div>
            <!-- /Container -->
			
</div>




	
<!-- Data Table JavaScript -->
    <script src="Assets/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="Assets/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="Assets/vendors/datatables.net-dt/js/dataTables.dataTables.min.js"></script>
    <script src="Assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="Assets/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="Assets/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="Assets/vendors/jszip/dist/jszip.min.js"></script>
    <script src="Assets/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="Assets/vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="Assets/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="Assets/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="Assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <!-- <script src="Assets/dist/js/dataTables-data.js"></script> -->

<script src="Controllers/JS/CtrlGestionCarreras.js"></script>