<!DOCTYPE html>
<!-- 
Template Name: Brunette - Responsive Bootstrap 4 Admin Dashboard Template
Author: Hencework
Contact: https://hencework.ticksy.com/

License: You must have a valid license purchased only from templatemonster to legally use the template for your project.
-->
<html lang="es">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>ArrayPay</title>
    <meta name="description" content="A responsive bootstrap 4 admin dashboard template by hencework" />

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="Assets/dist/img/Empresa/logo-array-pay2.png" type="image/x-icon">

      <!-- jQuery -->
    <script src="Assets/vendors/jquery/dist/jquery.min.js"></script>

    <!-- select2 CSS -->
    <link href="Assets/vendors/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" />
    
    <!-- Toastr CSS -->
    <link href="Assets/vendors/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css">

    <!-- Toggles CSS -->
    <link href="Assets/vendors/jquery-toggles/css/toggles.css" rel="stylesheet" type="text/css">
    <link href="Assets/vendors/jquery-toggles/css/themes/toggles-light.css" rel="stylesheet" type="text/css">
	
    <!-- Custom CSS -->
    <link href="Assets/dist/css/style.css" rel="stylesheet" type="text/css">
    
        
    <!-- Vue & axios -->
    <script src="Assets/vue.js"></script>
    <script src="Assets/axios.min.js"></script>

    
</head>

<body>
    <!-- Preloader -->
    <div class="preloader-it">
        <div class="loader-pendulums"></div>
    </div>
    <!-- /Preloader -->
	
	<!-- HK Wrapper -->
	<div class="hk-wrapper hk-alt-nav">

       <?php 
        if (isset($_SESSION["UserLoggedIn"]) && @$_SESSION["UserLoggedIn"]["estado"] == "ok") {
            require_once"Views/static/navbar.php";
            if (!isset($_GET["_RuTa_"])) {
                require_once"Views/modulos/dashboard/dashboard.client.php";
            }else{
                include"Views/modulos/".$_GET["_RuTa_"]."/".$_GET["_RuTa_"].".php";
                
            }
        }else{
            //require_once"Views/Login/login.php";
        }

       
       ?>
		
        <!-- Setting Panel -->
        <!-- /Setting Panel -->

        <!-- Main Content -->
        
        <!-- /Main Content -->
        	<!-- Footer -->
            <div class="hk-footer-wrap container">
                <footer class="footer">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <p>Desarrollado por<a href="https://array.com.co" class="text-dark" target="_blank">ARRAY TIC SAS</a> © <?php echo date("Y");?></p>
                        </div>
                        <!-- <div class="col-md-6 col-sm-12">
                            <p class="d-inline-block">Follow us</p>
                            <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-facebook"></i></span></a>
                            <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-twitter"></i></span></a>
                            <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-google-plus"></i></span></a>
                        </div> -->
                    </div>
                </footer>
            </div>
            <!-- /Footer -->
    </div>
    
    <!-- /HK Wrapper -->

   
    <!-- Jasny-bootstrap  JavaScript -->
    <script src="Assets/vendors/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>
    
    <!-- Bootstrap Core JavaScript -->
    <script src="Assets/vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="Assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Slimscroll JavaScript -->
    <script src="Assets/dist/js/jquery.slimscroll.js"></script>

    <!-- Fancy Dropdown JS -->
    <script src="Assets/dist/js/dropdown-bootstrap-extended.js"></script>

    <!-- FeatherIcons JavaScript -->
    <script src="Assets/dist/js/feather.min.js"></script>

    <!-- Toggles JavaScript -->
    <script src="Assets/vendors/jquery-toggles/toggles.min.js"></script>
    <script src="Assets/dist/js/toggle-data.js"></script>

    
	
	
	
	
    <!-- Init JavaScript -->
    <script src="Assets/dist/js/init.js"></script>
	
	
</body>

</html>