<?php 
require_once"../../Models/MdlCarreras.php";
require_once "../../Ext/carbon/vendor/autoload.php";
use Carbon\Carbon;
date_default_timezone_set('America/Bogota');
Carbon::setLocale('es');
$fechaActual = Carbon::now()->toDateTimeString();



if (isset($_POST["RegistrarCarrera"])) {
    sleep(1);
    if (preg_match('/^[a-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ ]+$/' , $_POST["titulo"]) ) {
        if (preg_match('/^[0-9]+$/', $_POST["precio"])) {
            
            if (preg_match('/^[a-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ ]+$/', $_POST["obs"]) || $_POST["obs"]== "") {
                

                $registrar  = MdlCarreras::RegistrarCarrera(
                    $_POST["titulo"],$_POST["precio"], $_POST["obs"], $fechaActual);
                if ($registrar) {
                    $REST["respuesta"] = "ok";
                }else{
                    $REST["respuesta"] = "err500";
                }


            }else{
                $REST["respuesta"] = "preg_match_err_obs";
            }

        }else{
            $REST["respuesta"] = "preg_match_err_precio";
        }

    }else{
        $REST["respuesta"] = "preg_match_err_titulo";
    }
    header("Content-Type: application/json");
    echo json_encode($REST);   
}



if (isset($_GET["CargarCarreras"])) {
    $carreras = MdlCarreras::CargarCarreras(null , null ,"DESC" , "id");
    //var_dump($carreras);

    $DatosJson = '{ "data": [ ';
        for ($i=0; $i < count($carreras) ; $i++) { 
            if ($carreras[$i]["estado"] == "activa") {
                $opciones = "<div class='btn-toolbar d-inline-block mb-25 mr-10' role='toolbar' aria-label='Toolbar with button groups'>" ;
                $opciones .= "<button idcarrera='".$carreras[$i]["id"]."'  type='button' class='editarCarrera btn btn-outline-light' data-toggle='tooltip' data-original-title='Editar información'><i class='fa fa-edit'></i></button>";
                $opciones .= "<button idcarrera='".$carreras[$i]["id"]."'  type='button' class='eliminarCarrera btn btn-outline-light' data-toggle='tooltip' data-original-title='Eliminar carrera'><i class='fa fa-times'></i></button>";
                $opciones .= "</div>";
    
    
                $DatosJson .= '[
                    "'.($carreras[$i]["id"]).'",
                    "'.($carreras[$i]["titulo"]).'",
                    "'.($carreras[$i]["obs"]).'",
                    "'.($carreras[$i]["precio"]).'",
                    "20",
                    "'.($opciones).'"
                ],';
            }
        }

        $DatosJson = substr($DatosJson , 0 , -1); ## en los JSON no pueden terminar con una (,) y si revisamos bien en el foreach siempre va a terminar con uns (,) y por eso estoy substrayendo eso para quedar con el ultimo registro sin (,)        
        $DatosJson .= ' ] }';

        echo $DatosJson;
}



if (isset($_GET["CargarCarrerasVueTodos"])) {
    $carreras = MdlCarreras::CargarCarreras(null , null ,"DESC" , "id");
    $carrerasDB = array ();
    foreach ($carreras as $key => $carrera) {
        if ($carrera["estado"] !== "eliminada") {
            array_push($carrerasDB , $carrera);
        }
    }

    $REST["respuesta"]  = $carrerasDB;
    header("Content-Type: application/json");
    echo json_encode($REST);   
}




if (isset($_POST["editar_carrera"])) {
    sleep(1);
    if (preg_match('/^[a-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ ]+$/' , $_POST["edit_titulo"]) ) {
        if (preg_match('/^[0-9]+$/', $_POST["edit_precio"])) {
            
            if (preg_match('/^[a-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ ]+$/', $_POST["edit_obs"]) || $_POST["edit_obs"]== "") {
                

                $editarCarrera  = MdlCarreras::EditarCarrera(
                    $_POST["id_carrera_seleccionada"] , $_POST["edit_titulo"],$_POST["edit_precio"], $_POST["edit_obs"]);
                if ($editarCarrera) {
                    $REST["respuesta"] = "ok";
                }else{
                    $REST["respuesta"] = "err500";
                }


            }else{
                $REST["respuesta"] = "preg_match_err_obs";
            }

        }else{
            $REST["respuesta"] = "preg_match_err_precio";
        }

    }else{
        $REST["respuesta"] = "preg_match_err_titulo";
    }
    header("Content-Type: application/json");
    echo json_encode($REST);   
}



if (isset($_POST["EliminarCarrera"])) {
    
    $PagosDeContado = MdlCarreras::CargarPagosDeContado($_POST["idCarrera"]);
    $PagosDiferidos = MdlCarreras::CargarDiferidos($_POST["idCarrera"]);
    

    if (count($PagosDeContado) == 0 && count($PagosDiferidos) == 0) {
        $eliminar = MdlCarreras::EliminarCarrera($_POST["idCarrera"]);
        $REST["respuesta"] = "ok";
    }else{
        $procesos_pendientes = 0;
        foreach ($PagosDeContado as $key => $pagoc) {
            if ($pagoc["estado"] == "pendiente") {
                $procesos_pendientes = $procesos_pendientes ++;
            }
        }

        foreach ($PagosDiferidos as $key => $pagod) {
            if ($pagoc["estado"] == "activo") {
                $procesos_pendientes = $procesos_pendientes ++;
            }
        }

        if ($procesos_pendientes > 0) {
            $REST["respuesta"] = "procesos_pendientes_actualmente";
        }else{
            $eliminar = MdlCarreras::EliminarCarrera($_POST["idCarrera"]);
            $REST["respuesta"] = "ok";
        }

    }
    

    
    header("Content-Type: application/json");
    echo json_encode($REST);   
}



?>

       
       