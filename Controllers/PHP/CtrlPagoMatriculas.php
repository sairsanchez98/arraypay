<?php 
session_start();
error_reporting(E_ALL);
require_once"../../Models/MdlPagosMatruculas.php";

if (isset($_GET["cargarPagosMatriculasUsuario"])) {
    
    $CONSULTA = PagosMatriculas::CargarPagosMatriculasUsuario(
        "id_usuario",$_SESSION["UserLoggedIn"]["user"]["id"],"DESC","id"
    );

    if (count($CONSULTA)>0) {
        $REST["respuesta"] = $CONSULTA;    
    }else if(count($CONSULTA)<=0){
        $REST["respuesta"] = "usuario_sin_registros";
    }else{
        $REST["respuesta"] = "ERR_500";
    }

    
    
    header("Content-Type: application/json");
    echo json_encode($REST);
}