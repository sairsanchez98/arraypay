<?php
    require_once"../../Models/MdlLogin.php";
    //require_once"../../Models/MdlUsuarios.php";

    // require_once "../../ext/carbon/vendor/autoload.php";
    // use Carbon\Carbon;
    // date_default_timezone_set('America/Bogota');
    // Carbon::setLocale('es');
    // $fechaActual = Carbon::now()->toDateTimeString();


    if (isset($_POST["peticionAccesoLogin"])) {
        sleep (1);
        ## SANEAMIENTO: 
        $USUARIO = $_POST["user"];
        $CONTRASENA = crypt($_POST["pass"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
        //$CONTRASENA = $_POST["pass"];
        
        ## analizamos que los CARACTERES ingresados por el visitante en el campo de USUARIO
        ## correspondan bien y no sean caracteres especiales ...
        if (preg_match('/^[a-zA-Z0-9]+$/', $USUARIO) || filter_var($_POST["user"], FILTER_VALIDATE_EMAIL)) 
        {
             ## Validamos el usuario (verificamos si es un email): 
            $ValidarUsuario = MdlLogin::ValidacionDeUsuario($USUARIO , "usuario");
            if ($ValidarUsuario) { ## si el usuario fue encontrado como EMAIL entonces...
                ##procedemos a validar la contraseña
                if ($ValidarUsuario[0]["contrasena"] == $CONTRASENA) {
                    $REST["respuesta"] = "acceso_ok";## si el usuario y contraseña ya fueron validados
                    ## y el acceso es OK entonces creamos la sesion:
                    session_start();
                    $USER_LOGUED = array("user" => $ValidarUsuario[0] ,  "estado"=>"ok"); 
                    ## aquí he puesto en un array el id del usuario logueado para tener la info de él
                    ## y también defino que el estado de la sesion es OK!
                    $_SESSION["UserLoggedIn"] = $USER_LOGUED; ## INGRESO LOS DATOS A LA SESION
                    $REST["_session_"] = $_SESSION["UserLoggedIn"];
                }else{
                    $REST["respuesta"] = "incorrect_pass";
                }
            }else{ 
                ## si  fue encontrado como usuario 
                    ## mandamos una respuesta de "USUARIO NO ENCONTRADO"
                    $REST["respuesta"] = "user_not_found";
            }
        }
        else
        {
            $REST["respuesta"] = "preg_match_err";
        }



        header("Content-Type: application/json");
        echo json_encode($REST);   
    }



















    if (isset($_POST["peticionAccesoRegistro"])) {
        
        ## PROCEDEMOS A VALIDAR CAMPO POR CAMPO: 


        ##validamos que los campos requeridos no estén vacios
        if ( (str_replace(" ","", $_POST["nombre"])) !== ""  && (str_replace(" ","", $_POST["apellidos"])) !== "" && 
            (str_replace(" ","", $_POST["correo"])) !== "" && !empty($_POST["pass"])) {
            
            ## valido el nombre  : 
            if (preg_match('/^[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ]+$/', str_replace(" ","", $_POST["nombre"] ) )) {
                

                ## valido los apellidos
                if (preg_match('/^[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ]+$/', str_replace(" ","", $_POST["apellidos"] ) )) {
                    

                    ## valido el correo electrónico
                    if (filter_var($_POST["correo"], FILTER_VALIDATE_EMAIL)) {
                        
                        ## validar si el correo se encuantra libre o ya está asociado a una cuenta
                        $validar_email_exist = MdlLogin::ValidacionDeUsuario($_POST["correo"] , "correo");
                        if ($validar_email_exist) {
                            $REST["respuesta"] = "correo_existente";
                        }else{ 
                            

                            ## validamos el NICK O USUARIO PERSONALIZADO
                            $nick_user = str_replace(" ","", $_POST["nick"] ) ;
                            if ($nick_user !== "") {  /// si el nick no viene vacio
                                if (preg_match('/^[a-zA-Z0-9]+$/',$_POST["nick"])) { # aqui tiene en cuenta los espacios ... si hay espacios no lo va a aceptar
                                    ## si ingresaron un usuario nick entonces devemos validar si ese usuario está en la db registrado
                                        $validar_nick_exist = MdlLogin::ValidacionDeUsuario($_POST["nick"] , "nick");
                                        if ($validar_nick_exist) { // si ya el usuario existe --- no se podrá registrar
                                            $REST["respuesta"] = "nick_existente";
                                            $registrar_nick = "no";    
                                            $error_validando_nick = true;
                                        }else{ // si pas+o todos los filtros ...
                                            $registrar_nick = "si";
                                            $error_validando_nick = false;
                                        }
                                }else{
                                    $registrar_nick = "no";
                                    $REST["respuesta"] = "preg_math_nick";
                                    $error_validando_nick = true;
                                }
                            }else{
                                $registrar_nick = "no";
                                $error_validando_nick = false;
                            }


                            if (!$error_validando_nick) { //si no hubo error reportado al momento de validar el nick 
                                ## entonces procedemos al sigueinte paso:

                                ## ahora encriptamos la contraseña:
                                    $CONTRASENA = crypt($_POST["pass"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');


                                    ### y ahora REGISTRAMOS AL USUARIO:
                                    // este es un registro simple.... luego cuando ya el usuario eté en la plataforma
                                    // podrá terminar de completar la información de su perfil-----
                                    if ($registrar_nick === "si") { $nickname = $_POST["nick"];}
                                    else{$nickname = "";}
                                    
                                    $RegistrarUsuario = MdlUsuarios::RegistroSimpleClientes(
                                        $_POST["nombre"],
                                        $_POST["apellidos"],
                                        $_POST["correo"],
                                        $nickname,
                                        $CONTRASENA,
                                        $fechaActual
                                    );

                                    if ($RegistrarUsuario) {
                                        # AGREGAR NOTIFICACIÓN DE REGISTRO DE USUARIO 

                                        ## SE CREA UNA SESIOÓN 
                                        ## para crear la sesion busco al usuario recien registrado con su correo: 
                                        // recordar que el correo es único para cada usuario y que por eso vamos a buscarlo 
                                        // con este dato una vez se haya registrado para así encontrar su ID correctamente
                                        $usuario_registrado = MdlLogin::ValidacionDeUsuario($_POST["correo"] , "correo");
                                        session_start();
                                        $USER_LOGUED = array("user" => $usuario_registrado[0] ,  "estado"=>"ok"); 
                                        // ## hasta aquí he puesto en un array el id del usuario logueado para tener la info de él
                                        // ## y también defino que el estado de la sesion es OK!
                                        $_SESSION["UserLoggedIn"] = $USER_LOGUED; ## INGRESO LOS DATOS A LA SESION
                                        $REST["respuesta"] = "OK";

                                    }else{
                                        $REST["respuesta"] = "err500"; // error interno
                                    }

                            }
                            


                        }

                    }else{
                        $REST["respuesta"] = "correo_incorrecto";
                    }


                }else{
                    $REST["respuesta"] = "preg_math_apellidos";
                }


            }else{
                $REST["respuesta"] = "preg_math_nombre";
            }







        }else{
            
            $REST["respuesta"] = "campos_vacios";
            if ( (str_replace(" ","", $_POST["nombre"])) == "") {$campos_vacios["nombre"] = true;}
            if ((str_replace(" ","", $_POST["apellidos"])) == "") {$campos_vacios["apellidos"] = true;}
            if ((str_replace(" ","", $_POST["correo"])) == "") {$campos_vacios["correo"] = true;}
            if (empty($_POST["pass"])) {$campos_vacios["pass"] = true;} // en la contraseña no eliminamos los espacios en blanco para la validación 
            ## porque el usuario es libre de colocar espacios en blanco dentro de su contraseña
            $REST["campos_vacios"] = $campos_vacios;

        }


        header("Content-Type: application/json");
        echo json_encode($REST);   
    }








    if (isset($_POST["destroySession"])) {
        session_start();
        $_SESSION["UserLoggedIn"]["estado"] = false;
        session_destroy();

        $REST["session"] = "destroy";

        header("Content-Type: application/json");
        echo json_encode($REST);   
    }


?>