<?php 
require_once"../../Models/Mdlestudiantes.php";
require_once "../../Ext/carbon/vendor/autoload.php";
use Carbon\Carbon;
date_default_timezone_set('America/Bogota');
Carbon::setLocale('es');
$fechaActual = Carbon::now()->toDateTimeString();


if (isset($_POST["RegistrarEstudiantes"])) {
    sleep(1);
    if (preg_match('/^[a-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ ]+$/' , $_POST["primer_nombre"]) && str_replace(" ","", $_POST["primer_nombre"]) !== "" ) {

        if (preg_match('/^[a-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ ]+$/' , $_POST["segundo_nombre"]) || str_replace(" ","", $_POST["segundo_nombre"]) == "" ) {

            if (preg_match('/^[a-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ ]+$/' , $_POST["primer_apellido"]) && str_replace(" ","", $_POST["primer_apellido"]) !== "" ) {

                if (preg_match('/^[a-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ ]+$/' , $_POST["segundo_apellido"]) && str_replace(" ","", $_POST["segundo_apellido"]) !== "" ) {

                    if (preg_match('/^[0-9 ]+$/', $_POST["cedula"]) && str_replace(" ","", $_POST["cedula"]) !== "" ) {

                        if (preg_match('/^[0-9]+$/', $_POST["celular"]) && str_replace(" ","", $_POST["celular"]) !== "" ) {
                            
                            if ( filter_var($_POST["correo"], FILTER_VALIDATE_EMAIL) ) {

                                    /// validamos si el correo se encuentra registrado 
                                    $validarCorreo = MdlEstudiantes::cargarEstudiantes("correo" , $_POST["correo"], "DESC" , "id");
                                    
                                    
                                    if (count($validarCorreo) > 0) {
                                        $REST["respuesta"] = "correo_existente";
                                    }else{ // si no se ha registrado .. no hay ningún problema y se procede al registro
                                            
                                        ## pero antes también debemos validar que la cedula no esté registrada
                                        ## ya que si está registrada entonces quiere deicr que el estudiante ya fue registrado XD JAJAJA
                                        $validarCC = MdlEstudiantes::cargarEstudiantes("cedula" , $_POST["cedula"], "DESC" , "id");
                                        if (count($validarCC) > 0) {
                                            $REST["respuesta"] = "cedula_existente";
                                        }else{
                                             //enqcripto contraseña
                                            $contrasena = crypt($_POST["contrasena"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
                                            //********************* */
                                            $registrar  = MdlEstudiantes::RegistrarEstudiantes(
                                                $_POST["primer_nombre"],$_POST["segundo_nombre"], $_POST["primer_apellido"], $_POST["segundo_apellido"], $_POST["cedula"], $_POST["celular"], $_POST["correo"], $contrasena, $fechaActual);
                                            if ($registrar) {
                                                $REST["respuesta"] = "ok";
                                            }else{
                                                $REST["respuesta"] = "err500";
                                            }
                                            //********* */
                                        }
                                       
                                    }
                                   


                            }else{
                                $REST["respuesta"] = "preg_match_err_correo";
                            }
                        }else{
                            $REST["respuesta"] = "preg_match_err_celular";
                        }
                    }else{
                        $REST["respuesta"] = "preg_match_err_cedula";
                    }

                }else{
                    $REST["respuesta"] = "preg_match_err_segundo_apellido";
                }

            }else{
                $REST["respuesta"] = "preg_match_err_primer_apellido";
            }

        }else{
            $REST["respuesta"] = "preg_match_err_segundo_nombre";
        }

    }else{
        $REST["respuesta"] = "preg_match_err_primer_nombre";
    }
    
    
    
    
    


    header("Content-Type: application/json");
    echo json_encode($REST);   
}




if (isset($_POST["EditarEstudiante"])) {
    sleep(1);
    if (preg_match('/^[a-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ ]+$/' , $_POST["primer_nombre"]) && str_replace(" ","", $_POST["primer_nombre"]) !== "" ) {

        if (preg_match('/^[a-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ ]+$/' , $_POST["segundo_nombre"]) || str_replace(" ","", $_POST["segundo_nombre"]) == "" ) {

            if (preg_match('/^[a-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ ]+$/' , $_POST["primer_apellido"]) && str_replace(" ","", $_POST["primer_apellido"]) !== "" ) {

                if (preg_match('/^[a-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ ]+$/' , $_POST["segundo_apellido"]) && str_replace(" ","", $_POST["segundo_apellido"]) !== "" ) {

                    if (preg_match('/^[0-9 ]+$/', $_POST["cedula"]) && str_replace(" ","", $_POST["cedula"]) !== "" ) {

                        if (preg_match('/^[0-9]+$/', $_POST["celular"]) && str_replace(" ","", $_POST["celular"]) !== "" ) {
                            
                            if ( filter_var($_POST["correo"], FILTER_VALIDATE_EMAIL) ) {

                                    // busco la información actual del estudiante : 
                                    $info_actual_estudiante = MdlEstudiantes::cargarEstudiantes("id" , $_POST["id_estudiante"], "DESC" , "id");

                                    if ($info_actual_estudiante[0]["correo"] == $_POST["correo"]) {
                                        # quiere decir que el admin no realizó cambios en el correo 
                                        $correos_encontrados = 0;
                                    }else{
                                         /// validamos si el correo se encuentra registrado 
                                        $validarCorreo = MdlEstudiantes::cargarEstudiantes("correo" , $_POST["correo"], "DESC" , "id");
                                        $correos_encontrados = count($validarCorreo) ;
                                    }
                                   
                                    
                                    if ($correos_encontrados > 0) {
                                        $REST["respuesta"] = "correo_existente";
                                    }else{ // si no se ha registrado .. no hay ningún problema y se procede al registro
                                            
                                        ## pero antes también debemos validar que la cedula no esté registrada
                                        ## ya que si está registrada entonces quiere deicr que el estudiante ya fue registrado XD JAJAJA
                                        if ($info_actual_estudiante[0]["cedula"] == $_POST["cedula"]) {
                                            $cedulas_encontradas = 0;
                                        }else{
                                            $validarCC = MdlEstudiantes::cargarEstudiantes("cedula" , $_POST["cedula"], "DESC" , "id");
                                            $cedulas_encontradas = count($validarCC);
                                        }

                                        if ($cedulas_encontradas > 0) {
                                            $REST["respuesta"] = "cedula_existente";
                                        }else{
                                             //enqcripto contraseña
                                             if ($_POST["nueva_contrasena"] !== "") {
                                                $contrasena = crypt($_POST["nueva_contrasena"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
                                                $EditarEstudiante = MdlEstudiantes::EditarInfoEstudiantesConContrasena(
                                                   $info_actual_estudiante[0]["id"],  $_POST["primer_nombre"],$_POST["segundo_nombre"], $_POST["primer_apellido"], $_POST["segundo_apellido"], $_POST["cedula"], $_POST["celular"], $_POST["correo"], $contrasena
                                                );
                                                

                                             }else{

                                                $EditarEstudiante = MdlEstudiantes::EditarInfoEstudiantesSinContrasena(
                                                    $info_actual_estudiante[0]["id"], $_POST["primer_nombre"],$_POST["segundo_nombre"], $_POST["primer_apellido"], $_POST["segundo_apellido"], $_POST["cedula"], $_POST["celular"], $_POST["correo"]
                                                );

                                             }

                                             if ($EditarEstudiante) {
                                                $REST["respuesta"] = "ok";
                                             }else{
                                                $REST["respuesta"] = "err500";
                                             }
                                            
                                            
                                        }
                                       
                                    }
                                   


                            }else{
                                $REST["respuesta"] = "preg_match_err_correo";
                            }
                        }else{
                            $REST["respuesta"] = "preg_match_err_celular";
                        }
                    }else{
                        $REST["respuesta"] = "preg_match_err_cedula";
                    }

                }else{
                    $REST["respuesta"] = "preg_match_err_segundo_apellido";
                }

            }else{
                $REST["respuesta"] = "preg_match_err_primer_apellido";
            }

        }else{
            $REST["respuesta"] = "preg_match_err_segundo_nombre";
        }

    }else{
        $REST["respuesta"] = "preg_match_err_primer_nombre";
    }
    
    
    
    

    header("Content-Type: application/json");
    echo json_encode($REST);   
}






if (isset($_GET["CargarEstudiantes"])) {
    $usuarios =  MdlEstudiantes::cargarEstudiantes(null, null, "DESC" , "id");
    //var_dump($carreras);

    $DatosJson = '{ "data": [ ';
        for ($i=0; $i < count($usuarios) ; $i++) { 
            if ($usuarios[$i]["estado"] == "activo" && $usuarios[$i]["rol"] == "cliente") {
                $opciones = "<div class='btn-toolbar d-inline-block mb-25 mr-10' role='toolbar' aria-label='Toolbar with button groups'>" ;
                $opciones .= "<button idcarrera='".$usuarios[$i]["id"]."'  type='button' class='editarEstudiante btn btn-outline-light' data-toggle='tooltip' data-original-title='Ver, editar & asignar'><i class='fa fa-edit'></i></button>";
                $opciones .= "<button idcarrera='".$usuarios[$i]["id"]."'  type='button' class='eliminarEstudiante btn btn-outline-light' data-toggle='tooltip' data-original-title='Eliminar usuario'><i class='fa fa-times'></i></button>";
                $opciones .= "</div>";
    
    
                $DatosJson .= '[
                    "'.($usuarios[$i]["id"]).'",
                    "'.($usuarios[$i]["cedula"]).'",
                    "'.($usuarios[$i]["primer_apellido"]. ' '. $usuarios[$i]["segundo_apellido"]. ' '. $usuarios[$i]["primer_nombre"]. ' '. $usuarios[$i]["segundo_nombre"]).'",
                    "'.($usuarios[$i]["primer_nombre"]).'",
                    "'.($usuarios[$i]["segundo_nombre"]).'",
                    "'.($usuarios[$i]["primer_apellido"]).'",
                    "'.($usuarios[$i]["segundo_apellido"]).'",
                    "'.($usuarios[$i]["celular"] . ' | '. $usuarios[$i]["correo"]).'",
                    "'.($opciones).'",
                    "'.($usuarios[$i]["celular"] ).'",
                    "'.($usuarios[$i]["correo"]).'"
                ],';
            }
        }

        $DatosJson = substr($DatosJson , 0 , -1); ## en los JSON no pueden terminar con una (,) y si revisamos bien en el foreach siempre va a terminar con uns (,) y por eso estoy substrayendo eso para quedar con el ultimo registro sin (,)        
        $DatosJson .= ' ] }';

        echo $DatosJson;
}




if (isset($_POST["calcularValorDePago"])) {
    
   
    if ($_POST["input"] == "total_personalizado" || $_POST["input"] == "numero_cuotas") {
        if ($_POST["tipo_de_pago"] == "diferido") {
            
            if (str_replace(" ","", $_POST["total_personalizado"]) == "") {
                $valor_total = $_POST["valor_total"];
            }else{
                $valor_total = $_POST["total_personalizado"];
            }

            $cuota_inicial_fija = $valor_total / $_POST["numero_cuotas"];
            if (str_replace(" ","", $_POST["cuota_inicial_personalizada"])  == "") {
                $valor_cuotas = $valor_total / $_POST["numero_cuotas"];
            }else if(str_replace(" ","", $_POST["cuota_inicial_personalizada"])  !== ""){
                $restante = $valor_total  - $_POST["cuota_inicial_personalizada"];
                $valor_cuotas = $restante / $_POST["numero_cuotas"];
            }else{
                $valor_cuotas = "";
            }

            $REST["cuota_inicial_fija"] = bcdiv(  $cuota_inicial_fija , '1', 0);
            $REST["valor_cuotas"] = bcdiv($valor_cuotas, '1', 0);

        }else{
            $REST["respuesta"] = "ok"; // no se hace nada porque el pago no es diferido
        }
    }



    else if($_POST["input"] == "cuota_inicial_personalizada" || $_POST["input"] == "cuota_inicial"){
        if (str_replace(" ","", $_POST["total_personalizado"]) == "") {
            $valor_total = $_POST["valor_total"];
        }else{
            $valor_total = $_POST["total_personalizado"];
        }

        if (str_replace(" ","", $_POST["cuota_inicial_personalizada"] ) !== "") {
            $restante = $valor_total - $_POST["cuota_inicial_personalizada"];
            $valor_cuotas = $restante / $_POST["numero_cuotas"];
            $REST["cuota_inicial_fija"] = "";
        }else{

            $valor_cuotas = $valor_total / $_POST["numero_cuotas"];
            $cuota_inicial_fija = $valor_total / $_POST["numero_cuotas"];
            $REST["cuota_inicial_fija"] = bcdiv(  $cuota_inicial_fija,  '1', 0);
        }
        


        $REST["valor_cuotas"] = bcdiv($valor_cuotas, '1', 0);
    }


    


    header("Content-Type: application/json");
    echo json_encode($REST);   
}









if (isset($_POST["AsignarCarrera"])) {

    ## DEFINIR NUMERO DE FACTURA
    sleep(1);
    if ($_POST["id_carrera"] !== "") {

        if ($_POST["tipo_de_pago"] !== "") {
            $facturasPagosDiferidos = MdlEstudiantes::CargarPagoCuotasDiferidos(null , null , "DESC", "id");
            $facturasPagosDeContado = MdlEstudiantes::CargarPagosDeCotando(null , null , "DESC", "id");

            if (count($facturasPagosDeContado) == 0 && count($facturasPagosDiferidos) == 0) {
                $NumeroNuevaFactura = 1;
            }else{
                ## la idea es que hay 2 tipos de facturas (de contado y diferidos) para definir el numero de factura que sigue 
                ## para un nuevo registro se debe analizar el ultimo registro de pagos de contado con el ultimo registro
                ## de pagos diferidos 
                if (@$facturasPagosDeContado[0]["numero_factura"] > @$facturasPagosDiferidos[0]["numero_factura"]) {
                    $NumeroNuevaFactura = ($facturasPagosDeContado[0]["numero_factura"] + 1);
                }else if (@$facturasPagosDeContado[0]["numero_factura"] < @$facturasPagosDiferidos[0]["numero_factura"]) {
                    $NumeroNuevaFactura = ($facturasPagosDiferidos[0]["numero_factura"] + 1);
                }
            }

            if (str_replace(" ","", $_POST["total_personalizado"]) == "") {
                $valor_total = $_POST["valor_total"];
            }else{
                $valor_total = $_POST["total_personalizado"];
            }

            if ($_POST["tipo_de_pago"] == "decontado") {
                $datos = array (
                    "fecha_registro" => $fechaActual,
                    "id_carrera" => $_POST["id_carrera"],
                    "id_usuario" =>  $_POST["id_estudiante"],
                    "numero_factura" => $NumeroNuevaFactura,
                    "valor_ordinario" => $valor_total
                );

                $registrar = MdlEstudiantes::AsignarCarreraAestudianteDECONTADO($datos);
                if ($registrar) {
                    $REST["respuesta"]  = "ok";
                }else{
                    $REST["respuesta"] = "err500";
                }
            }


            else if ($_POST["tipo_de_pago"] == "diferido") {
                if ($_POST["numero_cuotas"] !== "" && $_POST["numero_cuotas"] !== "0") {
                    if ($_POST["cuota_inicial"] !== "" || $_POST["cuota_inicial_personalizada"] !== "") {

                        if ($_POST["recurrencia"] !== "" && $_POST["recurrencia"] !== "0") {
                            if ($_POST["cuota_inicial_personalizada"] == "") {
                                $cuota_inicial = $_POST["cuota_inicial"]    ;
                            }else{
                                $cuota_inicial = $_POST["cuota_inicial_personalizada"];
                            }
                            

                            $datos = array (
                                "fecha_registro" => $fechaActual,
                                "id_carrera" => $_POST["id_carrera"],
                                "id_usuario" =>  $_POST["id_estudiante"],
                                "numero_factura" => $NumeroNuevaFactura,
                                "valor_ordinario" => $valor_total,
                                "numero_cuotas" => $_POST["numero_cuotas"],
                                "cuota_inicial" =>  $cuota_inicial,
                                "valor_cuotas" => $_POST["valor_cuotas"],
                                "recurrencia" => $_POST["recurrencia"]
                            );
                            $registrar = MdlEstudiantes::AsignarCarreraAestudianteDIFERIDO($datos);
                            if ($registrar) {

                                // /obtener el id del nuevo diferido realizado para cobrar 
                                //la cuota inicial (pago de cuotas)
                                $diferido_registrado = MdlEstudiantes::CargarPagosDiferidos("id_usuario", $datos["id_usuario"] , "DESC" , "id");
                                $registrar_pago_cuota_diferido = MdlEstudiantes::RegistrarPagoCuotaDiferido(
                                    $diferido_registrado[0]["id"] , 
                                    "cuota_inicial" , $datos["cuota_inicial"] , 
                                    "pendiente" , $datos["numero_factura"] , $fechaActual
                                ); ## CUOTA INICIAL

                                $REST["respuesta"] = "ok";
                            }else{
                                $REST["respuesta"] = $datos;
                            }


                        }else{
                            $REST["respuesta"] = "recurencia_no_valida";    
                        }

                    }else{
                        $REST["respuesta"] = "cuota_inicial_no_definida";
                    }    
                }else{
                    $REST["respuesta"] = "numero_cuotas_no_definido";
                }
            }


        }else{
            $REST["respuesta"] ="tipo_de_pago_no_definido";
        }
                
    }else{
        $REST["respuesta"] ="carrera_no_definida";
    }



    

    header("Content-Type: application/json");
    echo json_encode($REST);   
}