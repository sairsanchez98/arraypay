var pm = new Vue({
    el: "#pago_matriculas",
    data:{
        alert: false,
        pago_matriculas_DB : [],

        
       
    },
    methods: {
        cargar_pagos_matriculas : function (){
            axios.get("Controllers/PHP/CtrlPagoMatriculas.php?cargarPagosMatriculasUsuario")
            .then(function(response){
                console.log(response);
                let php = response.data;
                if(php.respuesta == "ERR_500" || php.respuesta == "usuario_sin_registros")
                {pm.alert="No se pudo cargar la lista de pagos de matrículas, recargue esta página para volver a intentarlo.";}
                else{
                    pm.pago_matriculas_DB = php.respuesta;
                }
            })
        },

        Pay : function(index_pago_matricula) { // esta función controla y valida el pago de las matriculas
            var handler = ePayco.checkout.configure({
                key: '00e75cfc6480207a7135e6efd3b4bb3d',
                test: false
            });
            // MATRICULA SELECCIONADA:
            let MatriculaSeleccionada = pm.pago_matriculas_DB[index_pago_matricula];

            /// EPAYCO VARIABLE PARA PAGO DE MATRÍCULA:
            let datos_pago={
                //Parametros compra (obligatorio)
                name: MatriculaSeleccionada["periodo"]+MatriculaSeleccionada["carrera"],
                description: MatriculaSeleccionada["periodo"]+MatriculaSeleccionada["carrera"],
                invoice: MatriculaSeleccionada["n_factura_usuario"],// numero de factura
                currency: "cop",
                amount: MatriculaSeleccionada["valor_ordinario"],
                tax_base: "0",
                tax: "0",
                country: "co",
                lang: "es",
      
                //Onpage="false" - Standard="true"
                external: "false",
      
      
                //Atributos opcionales
                confirmation: "http://secure2.payco.co/prueba_curl.php",
                response: "http://localhost/Unicor-PAY/pay-response",
      
                //Atributos cliente
                name_billing: "Andres Perez",
                //address_billing: "Carrera 19 numero 14 91",
                type_doc_billing: "cc",
                mobilephone_billing: "3050000000",
                number_doc_billing: "100000000",
      
               //atributo deshabilitación metodo de pago
                methodsDisable: ["SP"]
      
                }
       

            handler.open(datos_pago);

            

        }
    },
    mounted() {
        this.cargar_pagos_matriculas();
    },
})