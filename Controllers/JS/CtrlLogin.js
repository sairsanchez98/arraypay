var login = new Vue({
    el: "#login",
    data: {
        // datos de acceso: 
        user: "",
        pass: "",

        // estados del login:
        procesando : false,

        //alertas: 
            //alert_null_user 
            //alert_null_pass
            // alert_incorrect_pass
            //alert_email_not_found
        alert : false,
    },
    methods: {
        Login: function(){
            /// pongo el sistema en estado "procesando"
            this.procesando = true;
            this.alert = false;
            if(this.user != "" && this.pass != ""){


                let formdata = new FormData();
                formdata.append("peticionAccesoLogin" , true);
                formdata.append("user" , login.user);
                formdata.append("pass" , login.pass);

                axios.post("Controllers/PHP/CtrlLogin.php" , formdata)
                .then(function(response){
                    
                    let obj = response.data;
                    if(obj.respuesta == "preg_match_err"){
                        login.alert = "Usuario o correo no válido.";
                        login.procesando = false;
                    }

                    if(obj.respuesta == "user_not_found"){
                        login.alert = "Usuario o correo no encontrado.";
                        login.procesando = false;
                    }

                    if(obj.respuesta =="incorrect_pass"){
                        login.alert = "Contraseña incorrecta.";
                        login.procesando = false;
                    }
                    
                    if (obj.respuesta == "acceso_ok"){
                        location.reload();
                    }
                })


            }else if(this.user == "" && this.pass != ""){
                this.alert = "El campo de usuario es obligatorio.";
                this.procesando = false;
            }else if(this.pass == "" && this.user != ""){
                this.alert = "El campo de contraseña es obligatorio.";
                this.procesando = false;
            }else if(this.pass == "" && this.user == ""){
                this.alert = "Ingresa tu usuario y contraseña";
                this.procesando = false;
            }
        }
    },
    mounted() {
        
    },
})