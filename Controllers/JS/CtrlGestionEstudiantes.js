
var est = new Vue({
    el: "#gestion-estudiantes",
    data: {
        componentes: [{comp: "listado",estado: true} , {comp:"registrar" ,estado: false } , {comp:"editarEstudiante" ,estado: false } , {comp:"eliminarEstudiante" ,estado: false }],
        //datos de registro
        primer_nombre : "",
        segundo_nombre: "",
        primer_apellido : "",
        segundo_apellido: "",
        cedula: "",
        celular: "",
        correo : "",
        contrasena: "",


        alert: false,
        alert_class : "",
        alert_icon: "",


        input_primer_nombre : "",
        input_segundo_nombre: "",
        input_primer_apellido : "",
        input_segundo_apellido: "",
        input_cedula: "",
        input_celular: "",
        input_inputcelular: "",
        input_correo : "",
        input_contrasena: "",


        registro_procesando: false,

        
        estudiante_seleccionado : [],

        ////////////// variables para asignar carrera a los estudiantes
        alert_asignar_carrera : false,
        asignacion_procesando : false,
        editar_ver_info_est: false,
        CarrerasDB: [],
        id_carrera_seleccionada : "",
        CargarCarrerasVueTodos: "",
        valor_total : "000000000",
        total_personalizado : "",
        numero_cuotas  :  2,
        cuota_inicial: "",
        cuota_inicial_personalizada: "",
        valor_cuotas: "",
        recurrencia : "",
        tipo_de_pago : "",


        // editando la info de los estudiantes: 
        editar_procesando : false,
        nueva_contrasena: ""
        

        
    },
    methods: {
        HabilitarComponente : function (componente)
        {
            for (let index = 0; index < est.componentes.length; index++) {
                if(est.componentes[index]["comp"] == componente)
                {
                    if(est.componentes[index]["comp"] == "editarEstudiante"){ 
                        // si el componente es "editar o ver info estudiantes"
                        est.CargarCarreras(); // se cargan las carreras por si el ser admin desea agregar una carrera al estudiante
                        est.editar_ver_info_est= true;
                       
                    }else{est.editar_ver_info_est = false;}
                    est.componentes[index]["estado"] = true;
                }else{
                    est.componentes[index]["estado"] = false;
                }
            }
        },


        CargarEstudiantes : function ()
        {
            
            let get_ = "Controllers/PHP/CtrlEstudiantes.php?CargarEstudiantes";
            let tabla = $('#datable_1').DataTable( {
                "ajax" : get_,
                responsive: true,
                "bPaginate": true,
                "info":     true,
                "order": [[ 3, "desc" ]],
                "columns": [
                    { "data": 1 },
                    { "data": 2 },
                    { "data": 7 },
                    { "data": 8 }
                    
                    
                ],
                // más rapidez: 
                "deferRender":true,
                "retrieve" : true,
                "processing": true,
                "language": {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    }
                }
            } );
            tabla.ajax.reload();


            // capturar el evento EDITAR CARRERA:
            $("#datable_1 tbody").on("click" , "button.editarEstudiante",  function(){
                est.HabilitarComponente("editarEstudiante");
                var data  = tabla.row($(this).parents("tr")).data();
                est.estudiante_seleccionado = data;
                
            })

            // capturar el evento ELIMINAR CARRERA  
            $("#datable_1 tbody").on("click" , "button.eliminarEstudiante",  function(){
                est.HabilitarComponente("eliminarEstudiante");
                var data  = tabla.row($(this).parents("tr")).data();
                est.edit_titulo = data[1];
            })

          
        },


        CargarCarreras : function (){
            axios.get("Controllers/PHP/CtrlCarreras.php?CargarCarrerasVueTodos")
            .then(function(response){
                let php = response.data;
                est.CarrerasDB = php.respuesta;
            })

            $(".select_carreras").select2(); // se  el selector de carreras 
            $(".select_carreras").change(function(){  // seleccionar una carrera se debe mostrar toda su info
                let index_carrera_seleccionada = $("#carreras").val();
                if(index_carrera_seleccionada == ""){
                    est.id_carrera_seleccionada = "";
                }else{
                    est.id_carrera_seleccionada = est.CarrerasDB[index_carrera_seleccionada]["id"];
                    est.valor_total = est.CarrerasDB[index_carrera_seleccionada]["precio"];
                }
                //est.calcularValoresdePago();
            })
        },

        RegistrarEstudiante() {
            this.registro_procesando = true;
            this.alert = false;
            this.alert_class = "";
            this.alert_icon = "";

            this.input_primer_nombre = "";
            this.input_segundo_nombre = "";
            this.input_primer_apellido  = "";
            this.input_segundo_apellido = "";
            this.input_cedula =  "";
            this.input_celular =  "";
            this.input_inputcelular = "";
            this.input_correo  = "";
            this.input_contrasena = "";

            let formdata = new FormData();
            formdata.append("RegistrarEstudiantes" , true);
            formdata.append("primer_nombre" , est.primer_nombre);
            formdata.append("segundo_nombre" , est.segundo_nombre);
            formdata.append("primer_apellido" , est.primer_apellido);
            formdata.append("segundo_apellido" , est.segundo_apellido);
            formdata.append("cedula" , est.cedula);
            formdata.append("celular" , est.celular);
            formdata.append("correo" , est.correo);
            formdata.append("contrasena" , est.contrasena);

            axios.post("Controllers/PHP/CtrlEstudiantes.php" , formdata)
            .then(function(response){
                let php = response.data;
                // console.log(response);
                if(php.respuesta == "ok"){
                    est.registro_procesando = false;
                    est.alert = "Estudiante registrado con éxito";
                    est.alert_class = "alert-success";
                    est.alert_icon = "fa fa-check";
                    est.CargarEstudiantes();
                    est.HabilitarComponente("listado");
                    est.formatear_datos_registro();
                }else if (php.respuesta == "preg_match_err_correo"){
                    est.registro_procesando = false;
                    est.alert = "Correo electrónico no válido ";
                    est.alert_class = "alert-danger";
                    est.alert_icon = "fa fa-exclamation-triangle";
                    est.input_correo = "border-danger";
                }else if (php.respuesta == "correo_existente"){
                    est.registro_procesando = false;
                    est.input_correo = "border-danger";
                    est.alert = "El correo electrónico ya se encuentra registrado";
                    est.alert_class = "alert-danger";
                    est.alert_icon = "fa fa-exclamation-triangle";
                }else if (php.respuesta == "preg_match_err_celular"){
                    est.registro_procesando = false;
                    est.input_celular = "border-danger";
                    est.alert = "Número de celular no válido";
                    est.alert_class = "alert-danger";
                    est.alert_icon = "fa fa-exclamation-triangle";
                }else if (php.respuesta == "preg_match_err_cedula"){
                    est.registro_procesando = false;
                    est.input_cedula = "border-danger";
                    est.alert = "Número de cédula no válido";
                    est.alert_class = "alert-danger";
                    est.alert_icon = "fa fa-exclamation-triangle";
                }else if (php.respuesta == "cedula_existente"){
                    est.registro_procesando = false;
                    est.input_cedula = "border-danger";
                    est.alert = "El estudiante ya se encuentra registrado";
                    est.alert_class = "alert-danger";
                    est.alert_icon = "fa fa-exclamation-triangle";
                }else if (php.respuesta == "preg_match_err_segundo_apellido"){
                    est.registro_procesando = false;
                    est.input_segundo_apellido = "border-danger";
                    est.alert = "Segundo apellido no válido (este campo es requerido, no incluya caráteres especiales)";
                    est.alert_class = "alert-danger";
                    est.alert_icon = "fa fa-exclamation-triangle";
                }else if (php.respuesta == "preg_match_err_primer_apellido"){
                    est.registro_procesando = false;
                    est.input_primer_apellido = "border-danger";
                    est.alert = "Primer apellido no válido (este campo es requerido, no incluya caráteres especiales)";
                    est.alert_class = "alert-danger";
                    est.alert_icon = "fa fa-exclamation-triangle";
                }else if (php.respuesta == "preg_match_err_segundo_nombre"){
                    est.registro_procesando = false;
                    est.input_segundo_nombre = "border-danger";
                    est.alert = "Segundo nombre no válido (No incluya caráteres especiales)";
                    est.alert_class = "alert-danger";
                    est.alert_icon = "fa fa-exclamation-triangle";
                }else if (php.respuesta == "preg_match_err_primer_nombre"){
                    est.registro_procesando = false;
                    est.input_primer_nombre = "border-danger";
                    est.alert = "Primer nombre no válido (este campo es requerido, no incluya caráteres especiales)";
                    est.alert_class = "alert-danger";
                    est.alert_icon = "fa fa-exclamation-triangle";
                }
            })

        },


        EditarEstudiante(){
            est.editar_procesando = true;
            let formdata = new FormData();
            formdata.append("EditarEstudiante" , true);
            formdata.append("id_estudiante" , this.estudiante_seleccionado[0]);
            formdata.append("primer_nombre" , this.estudiante_seleccionado[3]);
            formdata.append("segundo_nombre" , this.estudiante_seleccionado[4]);
            formdata.append("primer_apellido" , this.estudiante_seleccionado[5]);
            formdata.append("segundo_apellido" , this.estudiante_seleccionado[6]);
            formdata.append("cedula" , this.estudiante_seleccionado[1]);
            formdata.append("celular" , this.estudiante_seleccionado[9]);
            formdata.append("correo" , this.estudiante_seleccionado[10]);
            formdata.append("nueva_contrasena" , this.nueva_contrasena);

            axios.post("Controllers/PHP/CtrlEstudiantes.php" , formdata)
            .then(function(response){
                console.log (response);
                let php = response.data;
                if(php.respuesta == "ok"){
                    est.editar_procesando = false;
                    est.alert = "La información del estudiante fue modificada con éxito.";
                    est.alert_class = "alert-success";
                    est.alert_icon = "fa fa-check";
                    est.CargarEstudiantes();
                    est.HabilitarComponente("listado");
                    est.formatear_datos_registro();
                }else if (php.respuesta == "preg_match_err_correo"){
                    est.editar_procesando = false;
                    est.alert = "Correo electrónico no válido ";
                    est.alert_class = "alert-danger";
                    est.alert_icon = "fa fa-exclamation-triangle";
                    est.input_correo = "border-danger";
                }else if (php.respuesta == "correo_existente"){
                    est.editar_procesando = false;
                    est.input_correo = "border-danger";
                    est.alert = "El correo electrónico ya se encuentra registrado";
                    est.alert_class = "alert-danger";
                    est.alert_icon = "fa fa-exclamation-triangle";
                }else if (php.respuesta == "preg_match_err_celular"){
                    est.editar_procesando = false;
                    est.input_celular = "border-danger";
                    est.alert = "Número de celular no válido";
                    est.alert_class = "alert-danger";
                    est.alert_icon = "fa fa-exclamation-triangle";
                }else if (php.respuesta == "preg_match_err_cedula"){
                    est.editar_procesando = false;
                    est.input_cedula = "border-danger";
                    est.alert = "Número de cédula no válido";
                    est.alert_class = "alert-danger";
                    est.alert_icon = "fa fa-exclamation-triangle";
                }else if (php.respuesta == "cedula_existente"){
                    est.editar_procesando = false;
                    est.input_cedula = "border-danger";
                    est.alert = "El estudiante ya se encuentra registrado";
                    est.alert_class = "alert-danger";
                    est.alert_icon = "fa fa-exclamation-triangle";
                }else if (php.respuesta == "preg_match_err_segundo_apellido"){
                    est.editar_procesando = false;
                    est.input_segundo_apellido = "border-danger";
                    est.alert = "Segundo apellido no válido (este campo es requerido, no incluya caráteres especiales)";
                    est.alert_class = "alert-danger";
                    est.alert_icon = "fa fa-exclamation-triangle";
                }else if (php.respuesta == "preg_match_err_primer_apellido"){
                    est.editar_procesando = false;
                    est.input_primer_apellido = "border-danger";
                    est.alert = "Primer apellido no válido (este campo es requerido, no incluya caráteres especiales)";
                    est.alert_class = "alert-danger";
                    est.alert_icon = "fa fa-exclamation-triangle";
                }else if (php.respuesta == "preg_match_err_segundo_nombre"){
                    est.editar_procesando = false;
                    est.input_segundo_nombre = "border-danger";
                    est.alert = "Segundo nombre no válido (No incluya caráteres especiales)";
                    est.alert_class = "alert-danger";
                    est.alert_icon = "fa fa-exclamation-triangle";
                }else if (php.respuesta == "preg_match_err_primer_nombre"){
                    est.editar_procesando = false;
                    est.input_primer_nombre = "border-danger";
                    est.alert = "Primer nombre no válido (este campo es requerido, no incluya caráteres especiales)";
                    est.alert_class = "alert-danger";
                    est.alert_icon = "fa fa-exclamation-triangle";
                }
            })
        },



        formatear_datos_registro(){
            this.primer_nombre = "";
            this.segundo_nombre = "";
            this.primer_apellido = "";
            this.segundo_apellido = "";
            this.cedula = "";
            this.celular = "";
            this.correo = "";
            this.contrasena = "";
        },


        AsignarCarreraAestudiante : function ()
        {
            est.asignacion_procesando = true;
            est.alert_asignar_carrera = false;
            let formdata = new FormData();
            formdata.append ("AsignarCarrera" , true);
            formdata.append ("tipo_de_pago" , this.tipo_de_pago);
            formdata.append ("id_carrera" ,this.id_carrera_seleccionada );
            formdata.append ("id_estudiante" ,this.estudiante_seleccionado[0] );
            formdata.append ("valor_total" , this.valor_total);
            formdata.append ("total_personalizado" , this.total_personalizado);
            formdata.append ("numero_cuotas" , this.numero_cuotas);
            formdata.append ("cuota_inicial" , this.cuota_inicial);
            formdata.append ("cuota_inicial_personalizada" , this.cuota_inicial_personalizada);
            formdata.append ("valor_cuotas" , this.valor_cuotas);
            formdata.append ("recurrencia" , this.recurrencia);

            axios.post("Controllers/PHP/CtrlEstudiantes.php" , formdata)
            .then(function(response){
                console.log(response)
                let php = response.data;
                if(php.respuesta == "ok"){
                    est.alert_asignar_carrera = "Se ha asignado la carrera exitosamente.";
                    est.alert_class = "alert-success";
                    est.alert_icon = "fa fa-check";
                    est.formatear_datos_asignacion_carrera();
                    est.asignacion_procesando = false;
                }else if(php.respuesta == "carrera_no_definida"){
                    est.alert_asignar_carrera = "Carrera no definida.";
                    est.alert_class = "alert-warning";
                    est.alert_icon = "fa fa-exclamation-triangle";
                    est.asignacion_procesando = false;
                }else if(php.respuesta == "tipo_de_pago_no_definido"){
                    est.alert_asignar_carrera = "Tipo de pago no definido.";
                    est.alert_class = "alert-warning";
                    est.alert_icon = "fa fa-exclamation-triangle";
                    est.asignacion_procesando = false;
                }else if (php.respuesta == "cuota_inicial_no_definida"){
                    est.alert_asignar_carrera = "Cuota inicial no definida.";
                    est.alert_class = "alert-warning";
                    est.alert_icon = "fa fa-exclamation-triangle";
                    est.asignacion_procesando = false;
                }else if(php.respuesta == "numero_cuotas_no_definido"){
                    est.alert_asignar_carrera = "Número de cuotas no definido.";
                    est.alert_class = "alert-warning";
                    est.alert_icon = "fa fa-exclamation-triangle";
                    est.asignacion_procesando = false;
                }else if(php.respuesta == "recurencia_no_valida"){
                    est.alert_asignar_carrera = "Recurrencia no permitida.";
                    est.alert_class = "alert-warning";
                    est.alert_icon = "fa fa-exclamation-triangle";
                    est.asignacion_procesando = false;
                }
            })

        },

        formatear_datos_asignacion_carrera : function (){
            $("#carreras").val("");
            this.id_carrera_seleccionada = "";
            this.valor_total = "000000000";
            this.total_personalizado = "";
            this.numero_cuotas  =  1;
            this.cuota_inicial= "";
            this.cuota_inicial_personalizada = "";
            this.valor_cuotas =  "";
            this.recurrencia  =  "";
            this.tipo_de_pago = "";
        },


        calcularValoresdePago : function (input){ // input hace referencia al campo que el user está modificando 
            if(input == "cuota_inicial_personalizada"){this.cuota_inicial = ""}else
            if(input == "cuota_inicial"){this.cuota_inicial_personalizada = ""}
            let formdata = new FormData ();
            
            formdata.append("calcularValorDePago" , true);
            formdata.append("tipo_de_pago" , this.tipo_de_pago);
            formdata.append("input" , input);
            formdata.append("valor_total" , this.valor_total);
            formdata.append("total_personalizado" , this.total_personalizado);
            formdata.append("numero_cuotas" , this.numero_cuotas);
            formdata.append("cuota_inicial_fija" , this.cuota_inicial);
            formdata.append("cuota_inicial_personalizada" , this.cuota_inicial_personalizada);
           axios.post("Controllers/PHP/CtrlEstudiantes.php" , formdata)
           .then(function (response){
                console.log(response);
                let php = response.data;
                if(est.cuota_inicial_personalizada == ""){
                    est.cuota_inicial = php.cuota_inicial_fija;
                }else{
                    est.cuota_inicial = "";
                }
                est.valor_cuotas = php.valor_cuotas;
           })
        }


    },

    mounted() {
        this.CargarEstudiantes();
    },
})