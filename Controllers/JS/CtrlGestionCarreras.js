var carrera = new Vue({
    el: "#gestion-carreras",
    data: {
        componentes: [{comp: "listado",estado: true} , {comp:"registrar" ,estado: false } , {comp:"editarCarrera" , estado: false }, {comp:"eliminarCarrera" , estado: false }],

        ///  DATOS DE REGISTRO :
        titulo : "",
        precio : "",
        obs : "",
        // DATOS DE EDICIÓN:
        id_carrera_seleccionada : "",
        edit_titulo: "",
        edit_precio: "",
        edit_obs: "",

        alert: false,
        alert_class : "",
        alert_icon: "",

        alert_input_titulo: "",
        alert_input_obs: "",
        alert_input_precio :"",

        registro_procesando: false,
        edicion_procesando: false,
        textbutton : "Registrar",
        textbuttonEdit : "Guardar cambios"


    },
    methods: {
        HabilitarComponente : function (componente)
        {
            for (let index = 0; index < this.componentes.length; index++) {
                if(this.componentes[index]["comp"] == componente)
                {
                    this.componentes[index]["estado"] = true;
                }else{
                    this.componentes[index]["estado"] = false;
                }
            }
        },

        CargarCarreras()
        {

            let get_ = "Controllers/PHP/CtrlCarreras.php?CargarCarreras";
            let tabla = $('#datable_1').DataTable( {
                "ajax" : get_,
                responsive: true,
                "bPaginate": true,
                "info":     true,
                "order": [[ 3, "desc" ]],
                "columns": [
                    { "data": 1 },
                    { "data": 2 },
                    { "data": 3 },
                    { "data": 4 },
                    { "data": 5 }
                ],
                // más rapidez: 
                "deferRender":true,
                "retrieve" : true,
                "processing": true,
                "language": {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    }
                }
            } );
            tabla.ajax.reload();


            // capturar el evento EDITAR CARRERA:
            $("#datable_1 tbody").on("click" , "button.editarCarrera",  function(){
                carrera.HabilitarComponente("editarCarrera");
                var data  = tabla.row($(this).parents("tr")).data();
                carrera.id_carrera_seleccionada = data[0];
                carrera.edit_titulo = data[1];
                carrera.edit_obs = data[2];
                carrera.edit_precio = data[3];
                
            })

            // capturar el evento ELIMINAR CARRERA  
            $("#datable_1 tbody").on("click" , "button.eliminarCarrera",  function(){
                carrera.HabilitarComponente("eliminarCarrera");
                var data  = tabla.row($(this).parents("tr")).data();
                carrera.id_carrera_seleccionada = data[0];
                carrera.edit_titulo = data[1];
            })

            


        },

        RegistrarCarrera : function ()
        {
           if(!this.registro_procesando){
            carrera.registro_procesando = true;
            this.textbutton = "Procesando...";

            this.alert = false;
            this.alert_icon = "";
            this.alert_class = "";
            this.alert_input_precio = "";
            this.alert_input_titulo = "";
            this.alert_input_obs = "";


            let formdata  = new FormData();
            formdata.append("RegistrarCarrera" , true);
            formdata.append("titulo" , this.titulo);
            formdata.append("obs" , this.obs);
            formdata.append("precio" , this.precio);

            axios.post("Controllers/PHP/CtrlCarreras.php" , formdata)
            .then(function(response){
                let php = response.data;
                console.log(php);
                if(php.respuesta == "ok")
                {   
                    carrera.registro_procesando = false;
                    carrera.titulo = ""; carrera.precio = ""; carrera.obs = "";
                    carrera.alert = "Carrera registrada con éxito."
                    carrera.alert_class = "alert-success";
                    carrera.alert_icon = "fa fa-check";
                    carrera.textbutton  = "Registrar";
                    carrera.HabilitarComponente("listado");
                    carrera.CargarCarreras();
                }else if(php.respuesta == "preg_match_err_obs"){
                    carrera.alert_input_obs = "border-danger";
                    carrera.textbutton  = "Registrar";
                    carrera.registro_procesando = false;
                    carrera.alert="Observación no valida (no incluya caracteres especiales)";
                    carrera.alert_class = "alert-danger";
                    carrera.alert_icon = "fa fa-exclamation-triangle";
                }else if(php.respuesta == "preg_match_err_precio"){
                    carrera.alert_input_precio = "border-danger";
                    carrera.textbutton  = "Registrar";
                    carrera.registro_procesando = false;
                    carrera.alert="valor del precio no valido";
                    carrera.alert_class = "alert-danger";
                    carrera.alert_icon = "fa fa-exclamation-triangle";
                }else if(php.respuesta == "preg_match_err_titulo"){
                    carrera.alert_input_titulo = "border-danger";
                    carrera.textbutton  = "Registrar";
                    carrera.registro_procesando = false;
                    carrera.alert="Titulo no valido (este campo es obligatorio, no incluya caracteres especiales)";
                    carrera.alert_class = "alert-danger";
                    carrera.alert_icon = "fa fa-exclamation-triangle";
                }
            })
           }
            
        },


        EditarCarrera : function (){

            if(!this.edicion_procesando){

                carrera.edicion_procesando = true;
                this.textbuttonEdit = "Procesando...";
    
                this.alert = false;
                this.alert_icon = "";
                this.alert_class = "";
                this.alert_input_precio = "";
                this.alert_input_titulo = "";
                this.alert_input_obs = "";

                let formdata = new FormData();
                formdata.append("editar_carrera" , true);
                formdata.append("id_carrera_seleccionada" , carrera.id_carrera_seleccionada);
                formdata.append("edit_titulo" , carrera.edit_titulo);
                formdata.append("edit_precio" , carrera.edit_precio);
                formdata.append("edit_obs" , carrera.edit_obs);
                axios.post("Controllers/PHP/CtrlCarreras.php" , formdata)
                .then(function(response){
                    let php = response.data;
                    if(php.respuesta == "preg_match_err_obs"){
                        carrera.alert_input_obs = "border-danger";
                        carrera.textbuttonEdit  = "Guardar cambios";
                        carrera.edicion_procesando = false;
                        carrera.alert="Observación no valida (no incluya caracteres especiales)";
                        carrera.alert_class = "alert-danger";
                        carrera.alert_icon = "fa fa-exclamation-triangle";
                    }else if(php.respuesta == "preg_match_err_precio"){
                        carrera.alert_input_precio = "border-danger";
                        carrera.textbuttonEdit  = "Guardar cambios";
                        carrera.edicion_procesando = false;
                        carrera.alert="valor del precio no valido";
                        carrera.alert_class = "alert-danger";
                        carrera.alert_icon = "fa fa-exclamation-triangle";
                    }else if(php.respuesta == "preg_match_err_titulo"){
                        carrera.alert_input_titulo = "border-danger";
                        carrera.textbuttonEdit  = "Guardar cambios";
                        carrera.edicion_procesando = false;
                        carrera.alert="Titulo no valido (este campo es obligatorio, no incluya caracteres especiales)";
                        carrera.alert_class = "alert-danger";
                        carrera.alert_icon = "fa fa-exclamation-triangle";
                    }else if (php.respuesta == "ok"){
                        carrera.edicion_procesando = false;
                        carrera.edit_titulo = ""; carrera.edit_precio = ""; carrera.edit_obs = "";
                        carrera.alert = "Los cambios se realizaron con éxito."
                        carrera.alert_class = "alert-success";
                        carrera.alert_icon = "fa fa-check";
                        carrera.textbuttonEdit  = "Guardar cambios";
                        carrera.HabilitarComponente("listado");
                        carrera.CargarCarreras();
                    }
                })
            }
        },


        EliminarCarrera(){
            let formdata = new FormData();
            formdata.append("EliminarCarrera" , true);
            formdata.append("idCarrera" ,  this.id_carrera_seleccionada);
            axios.post("Controllers/PHP/CtrlCarreras.php", formdata).then(function(response){
                let php = response.data;
                console.log(response);
                if(php.respuesta == "ok"){
                    carrera.HabilitarComponente("listado")
                    carrera.CargarCarreras();
                    carrera.alert = "Carrera eliminada";
                    carrera.alert_class = "alert-success";
                    carrera.alert_icon = "fa fa-check";
                }else if(php.respuesta == "procesos_pendientes_actualmente"){
                    carrera.HabilitarComponente("listado")
                    carrera.CargarCarreras();
                    carrera.alert = "La carrera no puede eliminarse, tiene procesos pendientes";
                    carrera.alert_class = "alert-warning";
                    carrera.alert_icon = "fa fa-exclamation-triangle";
                }
            })
        }


    },
    mounted() {
        this.CargarCarreras();
    },
})