-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 26-02-2020 a las 03:14:41
-- Versión del servidor: 5.7.29-0ubuntu0.18.04.1
-- Versión de PHP: 7.1.33-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `unicor_pay`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carreras`
--

CREATE TABLE `carreras` (
  `id` int(11) NOT NULL,
  `titulo` text NOT NULL,
  `precio` text NOT NULL,
  `obs` text NOT NULL,
  `fecha_registro` datetime NOT NULL,
  `estado` text NOT NULL COMMENT 'eliminada/activa'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos_diferidos`
--

CREATE TABLE `pagos_diferidos` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_carrera` int(11) NOT NULL,
  `fecha_registro` datetime NOT NULL,
  `estado` text NOT NULL COMMENT 'activo/cuota_inicial_pagada/pago_de_cuotas/total_pagado',
  `valor_total` int(11) NOT NULL,
  `valor_cuotas` int(11) NOT NULL,
  `valor_cuota_inicial` int(11) NOT NULL,
  `n_cuotas` int(11) NOT NULL,
  `recurrencia_de_pago` int(11) NOT NULL,
  `n_cuotas_pagadas` int(11) DEFAULT NULL COMMENT 'si el valor es 0 (cero) quiere decir que se pagó la cuota inicial PERSONALIZADA ... si la cuota inicial no es personalizada se hace el conteo normal desde 1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago_cuotas_diferidos`
--

CREATE TABLE `pago_cuotas_diferidos` (
  `id` int(11) NOT NULL,
  `id_diferido` int(11) NOT NULL,
  `tipo_cuota` text NOT NULL COMMENT 'cuota_inicial / cuota',
  `valor_cuota` int(11) NOT NULL,
  `estado` text NOT NULL,
  `fecha_de_pago` datetime DEFAULT NULL,
  `numero_factura` text NOT NULL,
  `fecha_registro` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago_de_contado`
--

CREATE TABLE `pago_de_contado` (
  `id` int(11) NOT NULL,
  `fecha_registro` datetime NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `numero_factura` text NOT NULL,
  `volante_de_pago` text,
  `valor_ordinario` int(11) NOT NULL,
  `valor_extraordinario` int(11) DEFAULT NULL,
  `fecha_pago_ordinario` date DEFAULT NULL,
  `fecha_pago_extraordinario` date DEFAULT NULL,
  `estado` text NOT NULL COMMENT 'pagado o pendiente',
  `periodo` text COMMENT 'ejemplo: 2020/SEM1 O 2020/SEM2',
  `id_carrera` text NOT NULL,
  `fecha_pago_realizado` date DEFAULT NULL,
  `ref_epayco` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `usuario` text NOT NULL,
  `correo` text NOT NULL,
  `celular` text NOT NULL,
  `contrasena` text NOT NULL,
  `id_usuario_pc` text COMMENT 'Este codigo es el del usuario en el sistema principal que provee la informacion del sistema de pagos ... ejemplo este campo tendrá el id de usuario de POWER CAMPUS .. de esa manera cuando un usuario realice un pago acá en este sistema ... power campus lo sabrá',
  `rol` text COMMENT 'cliente/admin',
  `cedula` int(11) NOT NULL,
  `primer_nombre` text NOT NULL,
  `segundo_nombre` text NOT NULL,
  `primer_apellido` text NOT NULL,
  `segundo_apellido` text NOT NULL,
  `fecha_registro` datetime NOT NULL,
  `estado` text NOT NULL COMMENT 'activo/eliminado'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `usuario`, `correo`, `celular`, `contrasena`, `id_usuario_pc`, `rol`, `cedula`, `primer_nombre`, `segundo_nombre`, `primer_apellido`, `segundo_apellido`, `fecha_registro`, `estado`) VALUES
(1, 'admin', 'sair@sair.com', '23123', '$2a$07$asxx54ahjppf45sd87a5auRajNP0zeqOkB9Qda.dSiTb2/n.wAC/2', NULL, 'admin', 213123, 'LUIS', '', 'QUICENO', '', '2020-02-23 21:29:58', 'activo');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `carreras`
--
ALTER TABLE `carreras`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pagos_diferidos`
--
ALTER TABLE `pagos_diferidos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pago_cuotas_diferidos`
--
ALTER TABLE `pago_cuotas_diferidos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pago_de_contado`
--
ALTER TABLE `pago_de_contado`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `carreras`
--
ALTER TABLE `carreras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pagos_diferidos`
--
ALTER TABLE `pagos_diferidos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pago_cuotas_diferidos`
--
ALTER TABLE `pago_cuotas_diferidos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pago_de_contado`
--
ALTER TABLE `pago_de_contado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
